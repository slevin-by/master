#include <CL\cl2.hpp>
#include <vector>
#include <iostream>
#include <fstream>
#include <random>

void GenerateRandomPoints(std::vector<double> &vX, std::vector<double> &vY, 
	size_t nNumber, double dWidth = 1000, double dHeight = 700)
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> disX(10, dWidth - 20);
	std::uniform_int_distribution<> disY(10, dHeight - 20);
	for (size_t i = 0; i < nNumber; i++)
	{
		vX.push_back(double(disX(gen)));
		vY.push_back(double(disY(gen)));
	}
}

void GenerateRandomKernels(std::vector<int> &vKernelLayout, std::vector<int> &assignment,
	size_t nKernelNumber, size_t nPointsNumber)
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, nPointsNumber - 1);
	for (size_t i = 0; i < nKernelNumber; i++)
	{
		int kernelIndex = dis(gen);
		vKernelLayout.push_back(kernelIndex);
		assignment[kernelIndex] = -1;
	}
}

void Test(cl::Context &context, cl::Program &program, cl::Device &device)
{
	std::vector<double> vx, vy;
	size_t np = 100, nk = 3;

	std::vector<int> layout;
	std::vector<int> assign;
	assign.resize(np);
	for (auto &it : assign)
		it = 0;

	GenerateRandomPoints(vx, vy, np);
	GenerateRandomKernels(layout, assign, nk, np);

	cl::Buffer bufferX(context, CL_MEM_READ_ONLY, sizeof(double) * np);
	cl::Buffer bufferY(context, CL_MEM_READ_ONLY, sizeof(double) * np);
	cl::Buffer bufferL(context, CL_MEM_READ_WRITE, sizeof(int) * nk);
	cl::Buffer bufferA(context, CL_MEM_READ_WRITE, sizeof(int) * np);
	cl::Buffer bufferB(context, CL_MEM_READ_WRITE, sizeof(int));

	double *x = new double[vx.size()];
	double *y = new double[vy.size()];
	int *l = new int[layout.size()];
	int *a = new int[assign.size()];
	int b = 0;

	for (size_t i = 0; i < np; i++)
	{
		x[i] = vx[i];
		y[i] = vy[i];
		a[i] = assign[i];
	}
	for (size_t i = 0; i < nk; i++)
		l[i] = layout[i];

	const int cnp = np;
	const int cnk = nk;

	cl::CommandQueue queue(context, device);
	queue.enqueueWriteBuffer(bufferX, CL_TRUE, 0, sizeof(double) * np, x);
	queue.enqueueWriteBuffer(bufferY, CL_TRUE, 0, sizeof(double) * np, y);
	queue.enqueueWriteBuffer(bufferL, CL_TRUE, 0, sizeof(int) * nk, l);
	queue.enqueueWriteBuffer(bufferA, CL_TRUE, 0, sizeof(int) * np, a);
	queue.enqueueWriteBuffer(bufferB, CL_TRUE, 0, sizeof(int), &b);

	cl::Kernel AssignKernels(program, "AssignKernels");
	AssignKernels.setArg(0, bufferX);
	AssignKernels.setArg(1, bufferY);
	AssignKernels.setArg(2, cnp);
	AssignKernels.setArg(3, bufferL);
	AssignKernels.setArg(4, cnk);
	AssignKernels.setArg(5, bufferA);
	AssignKernels.setArg(6, bufferB);

	size_t global_size = (2 * sizeof(double) * np) + (sizeof(int) * nk) +
		(sizeof(int) * np) + sizeof(int) + 1024;
	size_t local_size = 0;

	for (int i = 0; i < 10; i++)
	{
		queue.enqueueNDRangeKernel(AssignKernels, cl::NullRange, cl::NDRange(global_size), cl::NDRange(128));
		queue.enqueueReadBuffer(bufferB, CL_TRUE, 0, sizeof(int), &b);
		queue.enqueueReadBuffer(bufferL, CL_TRUE, 0, sizeof(int), l);

		for (int i = 0; i < nk; i++)
			printf("%d %d %d\n", l[0], l[1], l[2]);

		if (b == 0)
			break;
	}	
	
	queue.finish();

	printf("%d\n", b);
}

int main(int argc, char **argv)
{
	std::ifstream ifsFile("KMeans.cl");
	std::string szSourceCode(std::istreambuf_iterator<char>(ifsFile), (std::istreambuf_iterator<char>()));

	std::vector<cl::Platform> vPlatforms;
	cl::Platform::get(&vPlatforms);
	if (!vPlatforms.size())
	{
		std::cout << "ERROR: No platforms found." << std::endl;
		return 0;
	}

	cl::Platform platform = vPlatforms[0];
	std::cout << "Platform: " << platform.getInfo<CL_PLATFORM_NAME>() << std::endl;

	std::vector<cl::Device> vDevices;
	platform.getDevices(CL_DEVICE_TYPE_ALL, &vDevices);
	if (!vDevices.size())
	{
		std::cout << "ERROR: No devices found." << std::endl;
		return 0;
	}

	cl::Device device = vDevices[0];
	std::cout << "  Device: " << device.getInfo<CL_DEVICE_NAME>() << std::endl;

	cl::Context context({ device });
	cl::Program program(context, szSourceCode);
	if (program.build({ device }) != CL_SUCCESS)
	{
		std::cout << "ERROR: Build program failed." << std::endl;
		return 0;
	}

	Test(context, program, device);

	system("pause");
	return 0;
}
