double GetDistance(const double x1, const double y1,
				   const double x2, const double y2)
{
	double xd = pow(x1 - x2, 2);
	double yd = pow(y1 - y2, 2);
	double dd = sqrt(xd + yd);
	return dd;
}

__kernel void AssignKernels(__global const double *vecX,	// x of all points
							__global const double *vecY,	// y of all point
							const int nPointsNumber,	// number of points
							//__global double *vecKX,			// x of kernels
							//__global double *vecKY,			// y of kernels
							__global int *kernelLayout,	// x[0..nKernelNumber] - index of point that is kernel
							const int nKernelsNumber,	// number of kernels
							__global int *assignment,		// [0..nPointsNumber] - kernel index of the point. If kernel then -1
							__global int *bIsChanged)
{
	bIsChanged[0] = 0;
	
	for (int pi = 0; pi < nPointsNumber; pi++)
	{
		double minDist = 0xFFFFFFFF;

		if (assignment[pi] == -1) // if kernel
			continue;

		for (int ki = 0; ki < nKernelsNumber; ki++)
		{
			int indKern = kernelLayout[ki];
			double dist = GetDistance(vecX[pi], vecY[pi], vecX[indKern], vecY[indKern]);
			
			// if distance is less that minimal then assign a new kernel
			if (dist < minDist)
			{
				minDist = dist;

				assignment[pi] = -1;
				assignment[kernelLayout[ki]] = ki;
				kernelLayout[ki] = pi;

				bIsChanged[0] = 1;
			}
		}
	}	
}

//Infinity norm
double dist(int indVec, int indCenter, int nVecs, int nCenters, int vecLen,
           __global const double *vecMatrix,
           __global const double *centers)

{
    double dd = 0;
    int iNVecs = 0;
    int iNCenters = 0;
    for (int i = 0; i < vecLen; i++)
    {
        double dif = fabs(vecMatrix[indVec + iNVecs] - centers[indCenter + iNCenters]);

        dd = fmax(dif, dd);

        iNVecs += nVecs;
        iNCenters += nCenters;

    }
    return dd;
}

//global_size = number of vectors nVecs
__kernel void AssignToCenters(__global const double *vecMatrix,
                              __global const double *centers,
                              __global       int *assignment,
                              __constant     int *numCenters,
                              __constant     int *vectorLength,
                              __global       int *changed)
{
    int i = get_global_id(0);
    int nVecs = get_global_size(0);
   
    int nCenters = numCenters[0];
    int vecLen = vectorLength[0];
   
   
    double dd = 1.0E30f;
    int indMin = -1;
    for (int k = 0; k < nCenters; k++)
    {
        double localD = dist(i, k, nVecs, nCenters, vecLen, vecMatrix, centers);
        indMin = (localD<dd) ? k : indMin;
        dd =     (localD<dd) ? localD : dd;

    }

    if (assignment[i] != indMin)
    {
        changed[0] = 1;
    }
    assignment[i] = indMin;
}
