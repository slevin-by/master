double GetDistance(const double x1, const double y1,
				   const double x2, const double y2)
{
	double xd = pow(x1 - x2, 2);
	double yd = pow(y1 - y2, 2);
	double dd = sqrt(xd + yd);
	return dd;
}

__kernel void CalculateAllDistances(__global const double *vecX,	// x of all points
									__global const double *vecY,	// y of all point
									const int nPointsNumber,
									double *mapDist
								   )
{
	double dist = 0.0;
	for (int i = 0; i < nPointsNumber; i++)
	{
		for (int j = 0; j < nPointsNumber; j++)
		{
			dist = GetDistance(vecX[i], vecY[i], vecX[j], vecY[j]);
			mapDist[i * nPointsNumber + j] = dist;
		}
	}
}

__kernel void CalculateDistancesFor(const double X,	// x
									const double Y,	// y
									__global const double *vecX,	// x of all points
									__global const double *vecY,	// y of all point
									const int nPointsNumber,
									__global double *mapDist
								   )
{
	for (int i = 0; i < nPointsNumber; i++)
	{
		mapDist[i] = GetDistance(X, Y, vecX[i], vecY[i]);
	}
}

int isKernel(const double *kernels, int size, double point)
{
	for (int i = 0; i < size; i++)
	{
		if (kernels[i] == point)
			return 1;
	}
	return 0;
}

__kernel void AssignKernels(__global const double *vecX,	// x of all points
							__global const double *vecY,	// y of all point
							const int nPointsNumber,	// number of points
							__global int *kernelLayout,	// x[0..nKernelNumber] - index of point that is kernel
							const int nKernelsNumber,	// number of kernels
							__global int *assignment,		// [0..nPointsNumber] - kernel index of the point. If kernel then -1
							__global int *bIsChanged)
{
	bIsChanged[0] = 0;
	
	for (int pi = 0; pi < nPointsNumber; pi++)
	{
		double minDist = 0xFFFF;
		double dist = 0;

		if (isKernel(kernelLayout, nKernelsNumber, pi) == 1) // if kernel
			continue;

		for (int ki = 0; ki < nKernelsNumber; ki++)
		{
			dist = GetDistance(vecX[pi], vecY[pi], vecX[ki], vecY[ki]);
			
			// if distance is less that minimal then assign a new kernel
			if (dist < minDist)
			{
				minDist = dist;

				assignment[pi] = ki;
				kernelLayout[ki] = pi;
			}
		}
	}	
}

__kernel void RecalculateKernels(__global const double *vecX,	// x of all points
								 __global const double *vecY,	// y of all point
								 const int nPointsNumber,	// number of points
								 __global int *kernelLayout,	// x[0..nKernelNumber] - index of point that is kernel
								 const int nKernelsNumber,	// number of kernels
								 __global int *assignment,		// [0..nPointsNumber] - kernel index of the point. If kernel then -1
								 __global int *bIsChanged)
{
	int n = 0;

Loop:

	for (int pi = 0; pi < nPointsNumber; pi++)
	{
		double minDist = 0xFFFF;
		double dist = 0;

		if (isKernel(kernelLayout, nKernelsNumber, pi) == 1) // if kernel
			continue;

		for (int ki = 0; ki < nKernelsNumber; ki++)
		{
			dist = GetDistance(vecX[pi], vecY[pi], vecX[ki], vecY[ki]);
			
			// if distance is less that minimal then assign a new kernel
			if (dist < minDist)
			{
				minDist = dist;

				assignment[pi] = ki;
				kernelLayout[ki] = pi;
			}
		}
	}	

	bIsChanged[0] = 0;

	for (int ki = 0; ki < nKernelsNumber; ki++)
	{
		double dist = 0;
		double minDist = 0;
		int oldNum = 0;
		int newNum = 0;

		oldNum = kernelLayout[ki];
		for (int pi = 0; pi < nPointsNumber; pi++)
		{
			if ((assignment[pi] == ki) && (isKernel(kernelLayout, nKernelsNumber, pi) == 0))
			{
				dist = dist + GetDistance(vecX[oldNum], vecY[oldNum], vecX[pi], vecY[pi]);
			}
		}

		minDist = dist;

		for (int pi = 0; pi < nPointsNumber; pi++)
		{
			if ((assignment[pi] == ki) && (isKernel(kernelLayout, nKernelsNumber, pi) == 0))
			{
				dist = 0;
				for (int pii = 0; pii < nPointsNumber; pii++)
				{
					if (assignment[pii] == ki)
						dist = dist + GetDistance(vecX[pi], vecY[pi], vecX[pii], vecY[pii]);
				}
			}
			if (dist < minDist)
			{
				minDist = dist;
				newNum = pi;

				kernelLayout[ki] = pi;

				bIsChanged[0] = 1;

				break;
			}
		}

	}

	if (n == 1000)
		return;

	if (bIsChanged[0] == 1)
		goto Loop;
}
