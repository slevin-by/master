// WinAPI_Lab_1.cpp: ���������� ����� ����� ��� ����������.
//

#include <Windows.h>
#include <tchar.h>
#include <math.h>
#include <time.h>

#include "CLHelper.h"

#define MAX_LOADSTRING 100
#define WINWIDTH	1000
#define WINHIGTH	700

#define M_EXIT		0
#define M_GENERATE	1
#define M_PROCESS	2

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
ATOM MyRegisterClass(HINSTANCE);
BOOL InitInstance(HINSTANCE, int);
void SetInputInterface(HINSTANCE);
int GetRnd(int, int);
void malloc_all(int, int);
void MakeColors(int);
void GenerateRandomPoints(HDC);
void DrawRandomClasses(HDC, HBRUSH *);
void DrawPoint(HDC, HBRUSH, int, int);
void DrawClass(HDC, HBRUSH, int, int);
void DeleteClass(HDC, int, int);
void Computing(HDC);
void Generate(void);
void Run(void);
DWORD WINAPI ThreadGenerateProc(CONST LPVOID);
DWORD WINAPI ThreadButtonClick(CONST LPVOID);

// ���������� ����������:
HINSTANCE hInst;
HWND hWnd;
HWND hEdit1, hEdit2, hButton1;
char StrInput1[255] = { 0 };
char StrInput2[255] = { 0 };
RECT rect;
HBRUSH *hBrush;
HPEN PenWhite = CreatePen(PS_SOLID, 1, RGB(255, 255, 255));
HPEN PenBlack = CreatePen(PS_SOLID, 1, RGB(0, 0, 0));
HGDIOBJ previous; // for brush
TCHAR WinName[] = _T("Window Class");
int *classes;
int PointsNumber, ClassesNumber;
double *dist;
int *dep;
struct MYPOINTS {
	int x;
	int y;
	int myClass = -1;
	int isKernel = -1;
} *points;

/******************************************
 *     Distance table (OpenCL)
 *******************************************/
std::vector<std::vector<float>> g_table;

float GetDistance(int i, int j)
{
	return g_table[i][j];
}

int APIENTRY WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{
	MSG msg;

	if (!MyRegisterClass(hInst))
	{
		MessageBox(NULL, L"Cannot register a class!", L"Error", MB_OK);
		return 0;
	}

	if (!InitInstance(hInst, nCmdShow))
	{
		MessageBox(NULL, L"Cannot create a window!", L"Error", MB_OK);
		return 0;
	}

	SetInputInterface(hInst);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return msg.wParam;
}

ATOM MyRegisterClass(HINSTANCE hInst)
{
	WNDCLASSEX wc;

	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_DBLCLKS;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = NULL;
	wc.cbWndExtra = NULL;
	wc.hInstance = hInst;
	wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = WinName;
	wc.hIconSm = LoadIcon(NULL, IDI_WINLOGO);

	if (!RegisterClassEx(&wc))
		return 0;
	else
		return 1;
}

BOOL InitInstance(HINSTANCE hInst, int nCmdShow)
{
	hWnd = CreateWindow(WinName, L"�����, ������������ �1, ��������� 251003",
		WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, 0,
		CW_USEDEFAULT, 0, NULL, NULL, hInst, NULL);

	if (!hWnd)
		return FALSE;

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

void SetInputInterface(HINSTANCE hInst)
{
	/* Input UI start */
	hEdit1 = CreateWindowEx(WS_EX_CLIENTEDGE, L"EDIT", L"",
		WS_CHILD | WS_VISIBLE | WS_TABSTOP | ES_MULTILINE,
		450, 30, 100, 20, hWnd, (HMENU)102, GetModuleHandle(NULL), NULL);
	hEdit2 = CreateWindowEx(WS_EX_CLIENTEDGE, L"EDIT", L"",
		WS_CHILD | WS_VISIBLE | WS_TABSTOP | ES_MULTILINE,
		450, 60, 100, 20, hWnd, (HMENU)102, GetModuleHandle(NULL), NULL);
	hButton1 = CreateWindowEx(0, L"BUTTON", L"Run K-means",
		WS_CHILD | WS_VISIBLE | WS_BORDER,
		450, 90, 100, 20, hWnd, NULL, hInst, NULL);
	/* Input UI end */
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;
	HANDLE hThreads;

	switch (message)
	{
	case WM_CREATE:
		SetWindowPos(hWnd, HWND_BOTTOM, 0, 0, 1000, 700, SWP_NOMOVE);
		break;
	case WM_COMMAND:
		if ((HWND)lParam == hButton1)
		{
			GetWindowTextA(hEdit1, StrInput1, 100);
			GetWindowTextA(hEdit2, StrInput2, 100);
			PointsNumber = atoi(StrInput1);
			ClassesNumber = atoi(StrInput2);

			DestroyWindow(hEdit1);
			DestroyWindow(hEdit2);
			DestroyWindow(hButton1);

			hThreads = CreateThread(NULL, 0, &ThreadButtonClick, (LPVOID)NULL, 0, NULL);
			WaitForSingleObject(hThreads, INFINITE);
			CloseHandle(hThreads);

			MessageBox(hWnd, L"Done!", L"Finish", MB_OK);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: �������� ����� ��� ���������...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

int GetRnd(int min, int max)
{
	long rnd = 0;
	__asm
	{
		rdtsc
		xor eax, edx
		bswap eax
		mov rnd, eax
	}
	rnd = rnd % max;
	rnd = min + (rnd % (max - min));
	return rnd;
}

void malloc_all(int PointsNumber, int ClassNumber)
{
	points = (struct MYPOINTS *)malloc(PointsNumber * sizeof(struct MYPOINTS));
	for (int i = 0; i < PointsNumber; i++)
	{
		points[i].isKernel = -1;
		points[i].myClass = -1;
	}
	classes = (int *)malloc(ClassesNumber * sizeof(int));
	dist = (double *)malloc(ClassesNumber * sizeof(double));
	dep = (int *)malloc(PointsNumber * sizeof(int));
	hBrush = (HBRUSH *)malloc(ClassesNumber * sizeof(HBRUSH));
}

void MakeColors(int ClassesNumber)
{
	for (int i = 0; i < ClassesNumber; i++)
	{
		int x = abs(GetRnd(0, 255));
		int y = abs(GetRnd(0, 255));
		int z = abs(GetRnd(0, 255));
		hBrush[i] = CreateSolidBrush(RGB(x, y, z));
	}
}

void GenerateRandomPoints(HDC hdc)
{
	for (int i = 0; i < PointsNumber; i++)
	{
		points[i].x = abs(GetRnd(10, WINWIDTH - 20));
		points[i].y = abs(GetRnd(10, WINHIGTH - 20));
		Ellipse(hdc, points[i].x - 2, points[i].y - 2, points[i].x + 2, points[i].y + 2);
	}
}

void GenerateRandomClasses(HDC hdc, HBRUSH *hBrush)
{
	for (int i = 0; i < ClassesNumber; i++)
	{
		classes[i] = abs(GetRnd(0, PointsNumber - 1));
		points[classes[i]].isKernel = i;
		points[classes[i]].myClass = i;
	}
}

void DrawClasses(HDC hdc, HBRUSH *hBrush)
{
	for (int i = 0; i < ClassesNumber; i++)
	{
		previous = SelectObject(hdc, hBrush[i]);
		Ellipse(hdc, points[classes[i]].x - 10, points[classes[i]].y - 10, points[classes[i]].x + 10, points[classes[i]].y + 10);
		SelectObject(hdc, previous);
	}
}

void DrawPoint(HDC hdc, HBRUSH hBrush, int x, int y)
{
	SelectObject(hdc, PenBlack);
	previous = SelectObject(hdc, hBrush);
	Ellipse(hdc, x - 5, y - 5, x + 5, y + 5);
	SelectObject(hdc, previous);
}

void DrawClass(HDC hdc, HBRUSH hBrush, int x, int y)
{
	SelectObject(hdc, PenBlack);
	previous = SelectObject(hdc, hBrush);
	Ellipse(hdc, x - 10, y - 10, x + 10, y + 10);
	SelectObject(hdc, previous);
}

void DeleteClass(HDC hdc, int x, int y)
{
	SelectObject(hdc, PenWhite);
	previous = SelectObject(hdc, CreateSolidBrush(RGB(255, 255, 255)));
	Ellipse(hdc, x - 11, y - 11, x + 11, y + 11);
	SelectObject(hdc, previous);
}

void Computing(HDC hdc)
{
	int x1, x2, y1, y2;
	int min;

	for (int i = 0; i < PointsNumber; i++)
	{
		for (int j = 0; j < ClassesNumber; j++)
		{
			x1 = points[classes[j]].x;
			y1 = points[classes[j]].y;
			x2 = points[i].x;
			y2 = points[i].y;
			//dist[j] = sqrt(pow((float)(x1 - x2), 2) + pow((float)(y1 - y2), 2));
			dist[j] = GetDistance(classes[j], i);
		}

		min = 0;
		for (int j = 0; j < ClassesNumber; j++)
			if (dist[j] < dist[min])
				min = j;
		dep[i] = classes[min];

		points[i].myClass = min;
		DrawPoint(hdc, hBrush[min], points[i].x, points[i].y);
	}
}

void CreateDistancesMap()
{
	int *x = (int *)calloc(sizeof(int), PointsNumber);
	for (int i = 0; i < PointsNumber; i++)
		x[i] = points[i].x;

	int *y = (int *)calloc(sizeof(int), PointsNumber);
	for (int i = 0; i < PointsNumber; i++)
		y[i] = points[i].y;

	float *d = (float *)calloc(sizeof(float), PointsNumber);

	for (int i = 0; i < PointsNumber; i++)
	{
		CLHelper clHelper;

		memset(d, 0, PointsNumber * sizeof(float));
		clHelper.DistanceFor(x[i], y[i], x, y, PointsNumber, d);

		std::vector<float> vDist;
		vDist.resize(PointsNumber);
		for (int i = 0; i < PointsNumber; i++)
			vDist[i] = d[i];

		g_table.emplace_back(vDist);
	}
}

void Generate()
{
	HDC hdc = GetDC(hWnd);
	GetClientRect(hWnd, &rect);
	PointsNumber = 100;
	ClassesNumber = 5;

	malloc_all(PointsNumber, ClassesNumber);

	MakeColors(ClassesNumber);
	GenerateRandomPoints(hdc);
	GenerateRandomClasses(hdc, hBrush);

	CreateDistancesMap();

	Computing(hdc);

	for (int i = 0; i < ClassesNumber; i++)
		DrawClass(hdc, hBrush[i], points[classes[i]].x, points[classes[i]].y);
}

DWORD WINAPI ThreadGenerateProc(CONST LPVOID lpParam)
{
	Generate();
	Run();

	ExitThread(0);
}

DWORD WINAPI ThreadButtonClick(CONST LPVOID lpParam)
{
	HDC hdc;
	HANDLE hThreads;

	hdc = GetDC(hWnd);
	GetClientRect(hWnd, &rect);
	FillRect(hdc, &rect, (HBRUSH)(COLOR_WINDOW + 1));

	hThreads = CreateThread(NULL, 0, &ThreadGenerateProc, NULL, 0, NULL);
	WaitForSingleObject(hThreads, INFINITE);
	CloseHandle(hThreads);

	ExitThread(0);
}

void Run()
{
	HDC hdc = GetDC(hWnd);
	bool Finish = FALSE;
	bool ToStep1 = TRUE;

	while (Finish == FALSE) //main loop
	{
		// step 1//

		for (int i = 0; i < ClassesNumber; i++) //create array of kernels
		{
			for (int j = 0; j < PointsNumber; j++)
			{
				if (points[j].isKernel == i)
				{
					//Kernels[i] = Elements[j];
					//Kernels[i].numInElements = j;
					classes[i] = j;
					break;
				}
			}
		}
		for (int i = 0; i < PointsNumber; i++) //choose class of the point
		{
			double min = 99999999999;
			double distance;
			if (points[i].isKernel == -1)
			{
				for (int j = 0; j < ClassesNumber; j++)
				{
					//distance = (Elements[i].Pt.X - Kernels[j].Pt.X)*(Elements[i].Pt.X - Kernels[j].Pt.X) + (Elements[i].Pt.Y - Kernels[j].Pt.Y)*(Elements[i].Pt.Y - Kernels[j].Pt.Y);
					//OpenCL//distance = sqrt(pow((float)(points[i].x - points[classes[j]].x), 2) + pow((float)(points[i].y - points[classes[j]].y), 2));
					distance = GetDistance(i, classes[j]);
					if (distance < min)
					{
						min = distance;
						DeleteClass(hdc, points[classes[j]].x, points[classes[j]].y);
						points[i].myClass = j;
						//Elements[i].Pcolor = Kernels[j].Pcolor;
					}
				}
			}
		}

		//--start Drawing--------------------------------//
		for (int i = 0; i < PointsNumber; i++)
		{
			DrawPoint(hdc, hBrush[points[i].myClass], points[i].x, points[i].y);
			if (points[i].isKernel != -1)
			{
				//Data.Brush = 20;
				DrawClass(hdc, hBrush[points[i].myClass], points[i].x, points[i].y);
			}


			//Data.Color = Elements[i].Pcolor;
			//obj.Draw(Elements[i]);
			//Data.Brush = 3;
		}
		//--end Drawing--------------------------------//


		//step2//
		ToStep1 = FALSE;


		for (int i = 0; i < ClassesNumber; i++)
		{
			double distance = 0; //distance to points
			double min; //minimum distance
			int oldnum; //old kernel number
			int newnum; //new kernel number

						//--calculate the distance from kernel to points------// 

			oldnum = classes[i];
			for (int k = 0; k < PointsNumber; k++)
			{
				if ((points[k].myClass == i) && (points[k].isKernel == -1))
				{
					//distance += (Elements[oldnum].Pt.X - Elements[k].Pt.X)*(Elements[oldnum].Pt.X - Elements[k].Pt.X) + (Elements[oldnum].Pt.Y - Elements[k].Pt.Y)*(Elements[oldnum].Pt.Y - Elements[k].Pt.Y);
					//OpenCL//distance += sqrt(pow((float)(points[oldnum].x - points[k].x), 2) + pow((float)(points[oldnum].y - points[k].y), 2));
					distance += GetDistance(oldnum, k);
				}
			}
			min = distance;

			//--end calculatings----------------------------//

			//--calculate distance from points as a kernel--//
			for (int j = 0; j < PointsNumber; j++)
			{
				if ((points[j].myClass == i) && (points[j].isKernel == -1))
				{
					distance = 0;
					for (int k = 0; k < PointsNumber; k++)
					{
						if (points[k].myClass == i)
						{
							//distance += (Elements[j].Pt.X - Elements[k].Pt.X) * (Elements[j].Pt.X - Elements[k].Pt.X) + (Elements[j].Pt.Y - Elements[k].Pt.Y) * (Elements[j].Pt.Y - Elements[k].Pt.Y);
							//OpenCL//distance += sqrt(pow((float)(points[j].x - points[k].x), 2) + pow((float)(points[j].y - points[k].y), 2));
							distance += GetDistance(j, k);
						}
					}
					if (distance < min)
					{
						min = distance;
						newnum = j;
						points[oldnum].isKernel = -1;
						DeleteClass(hdc, points[oldnum].x, points[oldnum].y);
						points[newnum].isKernel = i;
						//points[newnum].myClass = i;
						DrawClass(hdc, hBrush[points[newnum].myClass], points[newnum].x, points[newnum].y);
						oldnum = j;
						ToStep1 = TRUE;
						break;
					}
				}
			}
			//--end calc----------------------------------------//

		}

		if (ToStep1 == FALSE)
		{
			Finish = TRUE;
			//break;
		}
	}

	//MessageBox(hWnd, L"Done!", L"Finish", MB_OK);
}