#pragma once

#include <iostream>
#include <fstream>

class CLInst
{
public:
	CLInst()
	{
		//get all platforms (drivers)
		std::vector<cl::Platform> all_platforms;
		cl::Platform::get(&all_platforms);
		if (all_platforms.size() == 0) {
			std::cout << " No platforms found. Check OpenCL installation!\n";
			exit(1);
		}
		default_platform = all_platforms[0];
		std::cout << "Using platform: " << default_platform.getInfo<CL_PLATFORM_NAME>() << "\n";

		//get default device of the default platform
		std::vector<cl::Device> all_devices;
		default_platform.getDevices(CL_DEVICE_TYPE_ALL, &all_devices);
		if (all_devices.size() == 0) {
			std::cout << " No devices found. Check OpenCL installation!\n";
			exit(1);
		}
		default_device = all_devices[0];
		std::cout << "Using device: " << default_device.getInfo<CL_DEVICE_NAME>() << "\n";


		context = cl::Context({ default_device });

		std::ifstream ifsFile("Adder.cl");
		std::string kernel_code(std::istreambuf_iterator<char>(ifsFile), (std::istreambuf_iterator<char>()));

		// kernel calculates for each element C=A+B
		//std::string kernel_code =
		//	"   void kernel simple_add(global const int* A, global const int* B, global int* C){       "
		//	"       C[get_global_id(0)]=A[get_global_id(0)]+B[get_global_id(0)];                 "
		//	"   }                                                                               ";
		sources.push_back({ kernel_code.c_str(),kernel_code.length() });

		program = cl::Program(context, sources);
		if (program.build({ default_device }) != CL_SUCCESS) {
			std::cout << " Error building: " << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(default_device) << "\n";
			exit(1);
		}
	}
	~CLInst()
	{

	}

	void Add(int *A, int *B, int size, int *C)
	{
		// create buffers on the device
		cl::Buffer buffer_A(context, CL_MEM_READ_WRITE, sizeof(int) * size);
		cl::Buffer buffer_B(context, CL_MEM_READ_WRITE, sizeof(int) * size);
		cl::Buffer buffer_C(context, CL_MEM_READ_WRITE, sizeof(int) * size);

		//create queue to which we will push commands for the device.
		cl::CommandQueue queue(context, default_device);

		//write arrays A and B to the device
		queue.enqueueWriteBuffer(buffer_A, CL_TRUE, 0, sizeof(int) * size, A);
		queue.enqueueWriteBuffer(buffer_B, CL_TRUE, 0, sizeof(int) * size, B);


		//run the kernel
		//alternative way to run the kernel
		cl::Kernel kernel_add = cl::Kernel(program, "simple_add");
		kernel_add.setArg(0, buffer_A);
		kernel_add.setArg(1, buffer_B);
		kernel_add.setArg(2, buffer_C);
		queue.enqueueNDRangeKernel(kernel_add, cl::NullRange, cl::NDRange(size), cl::NullRange);
		queue.finish();

		//read result C from the device to array C
		queue.enqueueReadBuffer(buffer_C, CL_TRUE, 0, sizeof(int) * 10, C);
	}

private:
	cl::Platform default_platform;
	cl::Device default_device;
	cl::Context context;
	cl::Program::Sources sources;
	cl::Program program;
};
