#ifndef _CL_HELPER_H_
#define _CL_HELPER_H_

#include <CL\cl2.hpp>
#include <vector>
#include <iostream>
#include <fstream>
#include <random>

class CLHelper
{
public:
	CLHelper()
		: m_bIsValid(false)
	{
		std::vector<cl::Platform> platforms;
		cl::Platform::get(&platforms);
		if (platforms.size() == 0)
			return;
		m_platform = platforms[0];

		std::vector<cl::Device> devices;
		m_platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);
		if (devices.size() == 0)
			return;
		m_device = devices[0];

		m_context = cl::Context({ m_device });

		std::ifstream ifsFile("KMeans.cl");
		std::string kernel_code(std::istreambuf_iterator<char>(ifsFile), (std::istreambuf_iterator<char>()));

		m_sources.push_back({ kernel_code.c_str(),kernel_code.length() });

		m_program = cl::Program(m_context, m_sources);
		if (m_program.build({ m_device }) != CL_SUCCESS)
			return;

		m_bIsValid = true;
	}
	
	bool IsValid()
	{
		return m_bIsValid;
	}

	void DistanceFor(int X, int Y, int *A, int *B, int size, OUT float *C)
	{
		cl::Buffer buffer_A(m_context, CL_MEM_READ_WRITE, sizeof(int) * size);
		cl::Buffer buffer_B(m_context, CL_MEM_READ_WRITE, sizeof(int) * size);
		cl::Buffer buffer_C(m_context, CL_MEM_READ_WRITE, sizeof(float) * size);

		cl::CommandQueue queue(m_context, m_device);

		queue.enqueueWriteBuffer(buffer_A, CL_TRUE, 0, sizeof(int) * size, A);
		queue.enqueueWriteBuffer(buffer_B, CL_TRUE, 0, sizeof(int) * size, B);

		cl::Kernel kernel_add = cl::Kernel(m_program, "calc_dist");
		kernel_add.setArg(0, X);
		kernel_add.setArg(1, Y);
		kernel_add.setArg(2, buffer_A);
		kernel_add.setArg(3, buffer_B);
		kernel_add.setArg(4, buffer_C);
		queue.enqueueNDRangeKernel(kernel_add, cl::NullRange, cl::NDRange(size), cl::NDRange(size / 100));
		queue.finish();

		queue.enqueueReadBuffer(buffer_C, CL_TRUE, 0, sizeof(float) * size, C);
	}

private:
	cl::Platform m_platform;
	cl::Device m_device;
	cl::Context m_context;
	cl::Program::Sources m_sources;
	cl::Program m_program;

	bool m_bIsValid;
};

#endif // _CL_HELPER_H_
