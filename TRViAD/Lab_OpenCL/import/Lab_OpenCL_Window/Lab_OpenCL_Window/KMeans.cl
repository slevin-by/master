float GetDistance(const int x1, const int y1,
				  const int x2, const int y2)
{
	return sqrt(pow((float)(x1 - x2), 2) + pow((float)(y1 - y2), 2));
}

void kernel calc_dist(const int X, const int Y, global const int* A, global const int* B, global float* C)
{
	C[get_global_id(0)] = GetDistance(X, Y, A[get_global_id(0)], B[get_global_id(0)]);
}