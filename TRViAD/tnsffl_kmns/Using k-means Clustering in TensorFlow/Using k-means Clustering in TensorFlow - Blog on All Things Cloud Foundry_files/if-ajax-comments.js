// 		  File : if-ajax-comments.js
// Description : JavaScript library for 'iF AJAX Comments for Wordpress'
//     Version : 2.9
//Requirements : jQuery -> [http://jquery.com]
// 		Author : Peter 'Toxane' Michael
// 	Author URI : http://www.flowdrops.com

jQuery.noConflict();

var showForm, disableForm, showLivePreview, compatContentPress, activityImage, activeColor, inactiveColor;
var css_comment, css_commentform, css_commentlist, css_respond;
var textNoName, textNoEmail, textInvalidEmail, textNoComment, textAddingComment, textCommentAdded, textCommentPreview;

var iFACScripts =
{   
	init: function()
	{
		iFACScripts.getVars();
	},
    
    getVars: function()
	{
		var url = iFAC_plugin_path+'ajax/ajaxGetVars.php';
		jQuery.getJSON(url, { reqAction: "getAllSettings" }, iFACScripts.parseVars);
	},
		
	parseVars: function(data)
	{
		showForm = data.showForm;
		disableForm = data.disableForm;
		showLivePreview = data.showLivePreview;
		activityImage = data.activityImagePath;		
		activeColor = data.activeColor;
        inactiveColor = data.inactiveColor;
        css_comment = data.css_comment;
        css_commentform = data.css_commentform;
		css_commentlist = data.css_commentlist;
		css_respond = data.css_respond;
		textNoName = data.textNoName;
        textNoEmail = data.textNoEmail;
		textInvalidEmail = data.textInvalidEmail;
		textNoComment = data.textNoComment;
		textAddingComment = data.textAddingComment;
		textCommentAdded = data.textCommentAdded;
		textCommentPreview = data.textCommentPreview;
		compatContentPress = data.compatContentPress;
		required_name = data.required_name;
		required_email = data.required_email;
		if(showLivePreview == 'checked'){iFACScripts.livePreview();}
		iFACScripts.setColors();
		iFACScripts.bindForm();
		compatAntispamBee = data.compatAntispamBee;
		blog_url = data.blog_url;

	},

	livePreview: function()
	{
		jQuery('#'+css_comment).one('focus',function()
		{	// http://www.learningjquery.com/2006/11/really-simple-live-comment-preview
			jQuery('#'+css_comment).parent().after('<div id="preview-box"><div class="comment-by">'+textCommentPreview+'</div><div id="live-preview"></div></div>');
		});
		var $comment = '';
		jQuery('#'+css_comment).keyup(function()
		{
			$comment = jQuery('#'+css_comment).val();
			$comment = $comment.replace(/\n/g, "<br />").replace(/\n\n+/g, '<br /><br />');
			jQuery('#live-preview').html( $comment );
		});
	},
	
	setColors: function()
	{
		jQuery('#author').focus(function(){jQuery(this).css('background', '#'+activeColor);}).blur(function(){jQuery(this).css('background', '#'+inactiveColor);});
		jQuery('#email').focus(function(){jQuery(this).css('background', '#'+activeColor);}).blur(function(){jQuery(this).css('background', '#'+inactiveColor);});
		jQuery('#url').focus(function(){jQuery(this).css('background', '#'+activeColor);}).blur(function(){jQuery(this).css('background', '#'+inactiveColor);});
		jQuery('#'+css_comment).focus(function(){jQuery(this).css('background', '#'+activeColor);}).blur(function(){jQuery(this).css('background', '#'+inactiveColor);});
	},

	bindForm: function()
	{
		jQuery('#submit').one("click", function()
		{
			jQuery('#'+css_commentform).before('<div id="activity" class="msgActivity"></div>');
		});

		jQuery('#'+css_commentform).bind("submit", function()
		{
			try
			{
				var xError = '', xFocus = '';
				
				var formInputs= [];
				jQuery(':input', this).each(function()
				{
					if (this.type == 'checkbox')
					{
						if (this.checked)
						{
							this.value = this.value;
						}
						else
						{
							this.value = '';
						}
					}
					formInputs.push(this.name + '=' + encodeURIComponent(this.value));
				});

				if(required_name == 'checked')
				{
					if(this.author && this.author.value === '')
					{
						xError += '- '+textNoName+'\n';
						this.author.focus();
					}
				}
				
				if(required_email == 'checked')
				{
					var noMail = false;
					if(this.email && this.email.value === '')
					{
						xError += '- '+textNoEmail+'\n';
						this.email.focus();
						noMail = true;
					}
	
					
					if(this.email && !(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(this.email.value)))
					{
						if (! noMail)
						{
							xError += '- '+textInvalidEmail+'\n';
							this.email.focus();
						}
					}
				}
				
				// Antispam Bee Plugin compatibility
                if(compatAntispamBee == 'checked')
				{
					var comment_md5 = jQuery.md5(blog_url).substr(0, 5);
					var comment_field = jQuery( "textarea[name='"+css_comment+"-"+comment_md5+"']" );
					if(comment_field.val() === '' || comment_field.val() == this.comment.title)
					{
						xError += '- '+textNoComment;
						comment_field.focus();
					}
				}
				else
				{
					if(this.comment.value === '' || this.comment.value == this.comment.title)
					{
						xError += '- '+textNoComment;
						this.comment.focus();
					}
				}
                
                if(xError !== '')
                {
                alert(xError);
					//jQuery('#'+css_commentform).before('<div id="msgError" class="msgError"></div>');
					//jQuery('#msgError').html(xError).show().fadeOut(300).fadeIn().fadeOut(4000);
                    xError = '';
					return false;
                }
				else
				{
					iFACScripts.addActivity();
					jQuery.ajax(
					{
						type: "POST",
						data: formInputs.join('&'),
						url: this.getAttribute('action'),
						success: iFACScripts.parseSuccess,
						error: iFACScripts.ajaxError
					});
				}
				return false;
			}
			catch(e)
			{
				// Normal postback in case of an error
				return true;	
			}
		});
		jQuery('#submit').after('<input type="hidden" id="is_ifac_comment" name="is_ifac_comment" value="true" />');
	},
	
	ajaxError: function(XMLHttpRequest, textStatus, errorThrown)
	{
		var errorMessage = 'textStatus: ';
		if (textStatus !== null){errorMessage += textStatus;}
		if (errorThrown !== null){errorMessage += ', errorThrown: ' + errorThrown;}
		iFACScripts.throwError(errorMessage);
	},
	
	parseSuccess: function(data)
	{
		// check for errors
		var errordata = data.split("|");
		is_error = errordata[0]; // If WP returned an error, the value of is_error is now 'error'
		
		if(is_error == 'error')
		{
			iFACScripts.throwError(errordata[1]);
			return;
		}
		
		tmpComment = data;
		
		if(jQuery('.'+css_commentlist).length > 0)
		{
			// The commentlist exists i.e. this is not the first comment
			// Another ugliest hack that did ever hack ?
			var the_parent_class = jQuery('#'+css_respond).parent().attr('class');
			if(the_parent_class.search(/depth-/i) != -1)
			{
				// Check if there's already replies, if not, add <ul class="children">
				if(jQuery('#'+css_respond).parent().children('.children').length)
				{
					// There are replies, add comment to the end of the list
					jQuery('#'+css_respond).parent().children('.children').append(tmpComment);
				}
				else
				{
					// First reply
					tmpComment = '<ul class="children">'+tmpComment+'</ul>';
					jQuery('#'+css_respond).parent().append(tmpComment);
				}
			}
			else
			{
				// Normal comment
				jQuery('.'+css_commentlist).append(tmpComment);
			}
		}
		else
		{
			// The commentlist doesn't exist, this is the first comment
			
			// Do we need to support the 'Content Press' Theme?
			if(compatContentPress == 'checked')
			{
				jQuery('div.postbox').before(jQuery(tmpComment).find('div.boxcomments'));
			}
			else
			{
				tmpComment = '<ol class="'+css_commentlist+'">'+tmpComment+'</ol>';
				jQuery('#'+css_respond).before(jQuery(tmpComment));
			}
		}
		
		iFACScripts.removeActivity();
		iFACScripts.clearInputs();
		
		if (window.AjaxEditComments)
		{
   			AjaxEditComments.init();
		}
	},
	
	addActivity: function()
	{
		jQuery('#msgError').remove();
		if (disableForm == 'checked')
		{	
			jQuery('#'+css_commentform+' *').attr("disabled","disabled");
		}
		else
		{
			jQuery('#'+css_commentform).fadeOut();
		}
		jQuery('#activity').text(textAddingComment).css('background', 'url('+activityImage+') no-repeat right');
	},
	
	removeActivity: function()
	{
		if(is_error !='error')
			jQuery('#commentform').text(textCommentAdded).css('background', '');

		if(showForm == 'checked')
		{
			jQuery('#'+css_commentform).fadeIn();
		}
		jQuery('#'+css_commentform+' *').removeAttr("disabled")
	},
	
	clearInputs: function()
	{
		jQuery('#comment').val('');
		jQuery('#live-preview').html('');
	},
	
	throwError: function(message)
	{
		iFACScripts.removeActivity();
		jQuery('#'+css_commentform).fadeIn();
		jQuery('#activity').html('').css('background', '').html('');
		jQuery('#'+css_commentform).before('<div id="msgError" class="msgError">'+message+'</div>').show();
	}
};
jQuery(document).ready(iFACScripts.init);
