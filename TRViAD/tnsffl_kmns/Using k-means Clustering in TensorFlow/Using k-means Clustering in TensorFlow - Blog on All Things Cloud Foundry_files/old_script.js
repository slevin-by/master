/**
 * Created by rulinski on 16.05.16.
 */
jQuery('#menu-item-10221').addClass('current-menu-parent');
jQuery('#menu-item-10228').addClass('current-menu-item');
jQuery(function () {
    if (jQuery(window).width() > 768) {

        var menu_bl = jQuery('#menu-main-menu');
        var submenu_bl = jQuery('.mainmenuholder-wihite div');
        var el_number = jQuery('#menu-main-menu .current-menu-parent>ul>li>a').length;

        submenu_bl.width(menu_bl.width() + 1);
        jQuery('#menu-main-menu .current-menu-parent>ul>li>a').each(function () {
            if (jQuery(this).closest('li').hasClass('current-menu-item')) jQuery(this).addClass('active');

            jQuery(this).width(menu_bl.width() / el_number).clone().appendTo(submenu_bl);
        });
        jQuery('#menu-main-menu .current-menu-parent>ul').remove();
        jQuery('.mainmenuholder-wihite div').find('a').each(function () {
            jQuery(this).html('<span>' + jQuery(this).text() + '</span>');
        });

        jQuery('.searchsubmittericon').on('click',function(){
            var len = jQuery(this).parent().find('input').val().length;
            if (len >= 3) {
                jQuery(this).closest('form').submit();
            } else {
                alert("Search term must be at least 3 characters in length");
            }
        });
        jQuery("#searchform input").keypress(function (event) {
            if (event.which == 13) {
                event.preventDefault();
                var len = jQuery(this).val().length;
                if (len >= 3) {
                    jQuery(this).closest('form').submit();
                } else {
                    alert("Search term must be at least 3 characters in length");
                }
            }
        });
        jQuery('#header_search').remove();
        //                jQuery('#searchform input:type:submit').remove();
        jQuery('#menu-item-5366 a').html('<img class="ubermenu-image ubermenu-image-size-full" src="https://altoros.com/blog/wp-content/uploads/2015/04/AltorosLogo_mini1.png" width="140" height="22" alt="AltorosLogo_mini1">');

    }

    jQuery(window).scroll(function () {
        var header_bar = jQuery('#mainmenuholder');
        var adminbar_h = (jQuery('#wpadminbar').length > 0) ? jQuery('#wpadminbar').height() : 0;
        if (jQuery(window).scrollTop() >= 45) {
            //console.log(header_bar);
            header_bar.animate({top: (adminbar_h - 40) + 'px'}, 50, 'linear');
        } else {
            header_bar.animate({top: adminbar_h + 'px'}, 30);
        }
    });

});
