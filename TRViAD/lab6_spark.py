import sys
from pyspark import SparkContext
from pyspark import SparkConf

# configuration
conf = SparkConf().setAppName('Lab_6_count_characters')
conf = conf.setMaster('local[2]')
sc = SparkContext(conf=conf)

lines = sc.textFile(sys.argv[1])
lineLength = lines.map(lambda s: len(s))
totalLength = lineLength.reduce(lambda a,b: a+b)
print totalLength