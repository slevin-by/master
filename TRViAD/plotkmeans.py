import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

n_points = 2000
n_clusters = 5

points = tf.constant(np.random.uniform(0, 10, (n_points, 2)))
kernels = tf.Variable(tf.slice(tf.random_shuffle(points), [0, 0], [n_clusters, -1]))

points_new_dim = tf.expand_dims(points, 0)
kernels_new_dim = tf.expand_dims(kernels, 1)

distances = tf.reduce_sum(tf.square(tf.subtract(points_new_dim, kernels_new_dim)), 2)
class_assignments = tf.argmin(distances, 0)

means = []
for c in range(n_clusters):
    means.append(tf.reduce_mean(tf.gather(points, 
	                                      tf.reshape(tf.where(tf.equal(class_assignments, c)),
                                                     [1,-1]
                                                    )
                                         ),
                                reduction_indices=[1]
                               )
                )

new_kernels = tf.concat(means, 0)

update_kernels = tf.assign(kernels, new_kernels)
init = tf.initialize_all_variables()

with tf.Session() as sess:
  sess.run(init)
  [_, old_kernels_values, points_values, assignment_values] = sess.run([update_kernels, kernels, points, class_assignments])
  while (True):
    [_, kernel_values, points_values, assignment_values] = sess.run([update_kernels, kernels, points, class_assignments])
    if (np.array_equal(old_kernels_values, kernel_values)): # if same shape, same elements values
    #if (np.array_equiv(old_kernels_values, kernel_values)): # if broadcastable shape, same elements values
    #if (np.allclose(old_kernels_values, kernel_values)): # if same shape, elements have close enough values
      break;
    else:
      old_kernels_values = kernel_values
 
  print ("kernels:\n", kernel_values)

plt.scatter(points_values[:, 0], points_values[:, 1], c=assignment_values, s=50, alpha=0.5)
plt.plot(kernel_values[:, 0], kernel_values[:, 1], 'kx', markersize=15)
plt.show()