import sys
from pyspark import SparkContext
from pyspark import SparkConf

conf = SparkConf().setAppName('Lab_6_count_characters')
conf = conf.setMaster('local[2]')
sc = SparkContext(conf=conf)

data=sc.textFile(sys.argv[1])
counts=data.flatMap(lambda x:[(c,1) for c in x]).reduceByKey(add)

print counts