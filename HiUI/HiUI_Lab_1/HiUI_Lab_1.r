setwd("C:/Users/root/Desktop/Study/Master/HiUI/HiUI_Lab_1")

require(MASS)
require(STATS)

analyse_correlation<-function(x,y)
{
  print(cor.test(x,y))
  dev.new()
  plot(x,y)
}
  
dat <- read.table("11-glass.txt")
analyse_correlation(dat[,2], dat[,5])

n <- 1000
a <- c(-1,-1)
r <- cbind(c(10,-2),c(-2,1))
dat1 <- mvrnorm(n,a,r)
analyse_correlation(dat1[,1], dat1[,2]) 

