setwd("C:/Users/root/Desktop/Study/Master/HiUI/HiUI_Lab_2")

require(MASS)

analyse_clust <- function(x, y, clazz)
{
  k <- length(unique(clazz))
  clust <- kmeans(cbind(x, y), k)
  print(clust)
  dev.new()
  plot(x, y, col=as.factor(clazz))
  dev.new()
  plot(x, y, col=as.factor(clust$cluster))
  points(clust$centers, col=1:length(clust$centers), pch=4, cex=2)
}

dat <- read.table("11-glass.txt")
analyse_clust(dat[,2], dat[,5], as.factor(dat[,11]))

n1 <- 100
a1 <- c(-1, 1)
r1 <- cbind(c(2, -1), c(-1, 1))
n2 <- 1000
a2 <- c(-4, -2)
r2 <- cbind(c(2, -0.1), c(-0.1, 1))
dat <- rbind(mvrnorm(n1, a1, r1), mvrnorm(n2, a2, r2))
analyse_clust(dat[,1], dat[,2], c(rep(1, n1), rep(2, n2)))