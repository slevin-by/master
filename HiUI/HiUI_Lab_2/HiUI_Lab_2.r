setwd("C:/Users/root/Desktop/Study/Master/HiUI/HiUI_Lab_2")

require(MASS)

analyse_regression <- function(x, y)
{
  model <- lm(y ~ x)
  print(summary(model))
  dev.new()
  plot(x, y)
  abline(model)
}

dat <- read.table("11-glass.txt")
analyse_regression(dat[,2], dat[,5])

n <- 1000
a <- 0.5
b <- 0.1
s2 <- 0.1 
x <- seq(0.0, 1.0, length=n)
y <- a * x + b + rnorm(n, 0, s2)
analyse_regression(x, y)