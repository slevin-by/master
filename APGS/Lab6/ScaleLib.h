#ifndef _SCALE_LIB_H_
#define _SCALE_LIB_H_

#ifndef SCALE_API
#define SCALE_API extern "C" __declspec(dllimport)
#endif // SCALE_API

SCALE_API int ResizeBmp(const char *szSrcFilePath, const char *szDstFilePath, double fScale);

#endif // _SCALE_LIB_H_
