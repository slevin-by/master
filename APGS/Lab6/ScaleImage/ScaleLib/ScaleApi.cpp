#include "stdafx.h"

#include "ScaleApi.h"

bool LoadImageEx(const char *fileName, SImageData& imageData)
{
	// open the file if we can
	FILE *file;
	file = fopen(fileName, "rb");
	if (!file)
		return false;

	// read the headers if we can
	BITMAPFILEHEADER header;
	BITMAPINFOHEADER infoHeader;
	if (fread(&header, sizeof(header), 1, file) != 1 ||
		fread(&infoHeader, sizeof(infoHeader), 1, file) != 1 ||
		header.bfType != 0x4D42 || infoHeader.biBitCount != 24)
	{
		fclose(file);
		return false;
	}

	infoHeader.biSizeImage = infoHeader.biHeight * infoHeader.biWidth * 3;

	// read in our pixel data if we can. Note that it's in BGR order, and width is padded to the next power of 4
	imageData.m_pixels.resize(infoHeader.biSizeImage);
	fseek(file, header.bfOffBits, SEEK_SET);
	if (fread(&imageData.m_pixels[0], imageData.m_pixels.size(), 1, file) != 1)
	{
		fclose(file);
		return false;
	}

	imageData.m_width = infoHeader.biWidth;
	imageData.m_height = infoHeader.biHeight;

	imageData.m_pitch = imageData.m_width * 3;
	if (imageData.m_pitch & 3)
	{
		imageData.m_pitch &= ~3;
		imageData.m_pitch += 4;
	}

	fclose(file);
	return true;
}

bool SaveImage(const char *fileName, const SImageData &image)
{
	// open the file if we can
	FILE *file;
	file = fopen(fileName, "wb");
	if (!file)
		return false;

	// make the header info
	BITMAPFILEHEADER header;
	BITMAPINFOHEADER infoHeader;

	header.bfType = 0x4D42;
	header.bfReserved1 = 0;
	header.bfReserved2 = 0;
	header.bfOffBits = 54;

	infoHeader.biSize = 40;
	infoHeader.biWidth = image.m_width;
	infoHeader.biHeight = image.m_height;
	infoHeader.biPlanes = 1;
	infoHeader.biBitCount = 24;
	infoHeader.biCompression = 0;
	infoHeader.biSizeImage = image.m_pixels.size();
	infoHeader.biXPelsPerMeter = 0;
	infoHeader.biYPelsPerMeter = 0;
	infoHeader.biClrUsed = 0;
	infoHeader.biClrImportant = 0;

	header.bfSize = infoHeader.biSizeImage + header.bfOffBits;

	// write the data and close the file
	fwrite(&header, sizeof(header), 1, file);
	fwrite(&infoHeader, sizeof(infoHeader), 1, file);
	fwrite(&image.m_pixels[0], infoHeader.biSizeImage, 1, file);
	fclose(file);
	return true;
}

const uint8* GetPixelClamped(const SImageData& image, int x, int y)
{
	CLAMP(x, 0, image.m_width - 1);
	CLAMP(y, 0, image.m_height - 1);
	return &image.m_pixels[(y * image.m_pitch) + x * 3];
}

std::array<uint8, 3> SampleNearest(const SImageData& image, float u, float v)
{
	// calculate coordinates
	int xint = int(u * image.m_width);
	int yint = int(v * image.m_height);

	// return pixel
	auto pixel = GetPixelClamped(image, xint, yint);
	std::array<uint8, 3> ret;
	ret[0] = pixel[0];
	ret[1] = pixel[1];
	ret[2] = pixel[2];
	return ret;
}

void ResizeImage(const SImageData &srcImage, SImageData &destImage, float scale)
{
	destImage.m_width = long(float(srcImage.m_width)*scale);
	destImage.m_height = long(float(srcImage.m_height)*scale);
	destImage.m_pitch = destImage.m_width * 3;
	if (destImage.m_pitch & 3)
	{
		destImage.m_pitch &= ~3;
		destImage.m_pitch += 4;
	}
	destImage.m_pixels.resize(destImage.m_pitch*destImage.m_height);

	uint8 *row = &destImage.m_pixels[0];
	for (int y = 0; y < destImage.m_height; ++y)
	{
		uint8 *destPixel = row;
		float v = float(y) / float(destImage.m_height - 1);
		for (int x = 0; x < destImage.m_width; ++x)
		{
			float u = float(x) / float(destImage.m_width - 1);
			std::array<uint8, 3> sample;

			sample = SampleNearest(srcImage, u, v);

			destPixel[0] = sample[0];
			destPixel[1] = sample[1];
			destPixel[2] = sample[2];
			destPixel += 3;
		}
		row += destImage.m_pitch;
	}
}
