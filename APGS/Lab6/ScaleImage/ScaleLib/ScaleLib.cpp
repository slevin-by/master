#include "stdafx.h"

#include "ScaleApi.h"

#define SCALE_API extern "C" __declspec(dllexport)

SCALE_API int ResizeBmp(const char *szSrcFilePath, const char *szDstFilePath, double fScale)
{
//	char *szDstFilePath = (char *)calloc(strlen(szFilePath) + 2, sizeof(char));
//	szDstFilePath[strlen(szFilePath)] = '1';

	SImageData srcImage;
	if (LoadImageEx(szSrcFilePath, srcImage))
	{
		SImageData destImage;
		ResizeImage(srcImage, destImage, (float)fScale);
		if (SaveImage(szDstFilePath, destImage))
			return 1;
		else
			return 0;
	}
	else
		return 0;
}

