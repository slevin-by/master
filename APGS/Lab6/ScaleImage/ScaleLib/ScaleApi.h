#ifndef _SCALE_API_H_
#define _SCALE_API_H_

#include <stdio.h>
#include <stdint.h>
#include <array>
#include <vector>
#include <windows.h>

#define CLAMP(v, min, max) if (v < min) { v = min; } else if (v > max) { v = max; } 

typedef uint8_t uint8;

struct SImageData
{
	SImageData()
		: m_width(0)
		, m_height(0)
	{ }

	long m_width;
	long m_height;
	long m_pitch;
	std::vector<uint8> m_pixels;
};

bool LoadImageEx(const char *fileName, SImageData& imageData);
bool SaveImage(const char *fileName, const SImageData &image);
void ResizeImage(const SImageData &srcImage, SImageData &destImage, float scale);

#endif // _SCALE_API_H_
