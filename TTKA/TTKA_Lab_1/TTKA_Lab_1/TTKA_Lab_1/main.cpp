#include <iostream>
#include "Grammary.h"

void Test_Type0()
{
	CGrammary grammary;

	// test zero

	// Vt - small
	// Vn - big

	std::vector<std::string> vt = { "a", "b", SZ_EPSILON, };
	std::vector<std::string> vn = { "A", "B", "C", "D", "F", };

	grammary.setS("S");
	grammary.setVt(vt);
	grammary.setVn(vn);

	grammary.addOneP("S", "aaCFD");
	grammary.addOneP("AD", "D");
	grammary.addOneP("F", "AFB");
	grammary.addOneP("F", "AB");
	grammary.addOneP("Cb", "bC");
	grammary.addOneP("AB", "bBA");
	grammary.addOneP("CB", "C");
	grammary.addOneP("Ab", "bA");
	grammary.addOneP("bCD", SZ_EPSILON);

	std::cout << "Expected: Type0" << std::endl;
	std::cout << "  Formal: " << grammary.alalyze() << std::endl;
	std::cout << "  Result: " << (eType0 == grammary.getType()) << std::endl;
}

void Test_Type1()
{
	CGrammary grammary;

	// test context-dependent
	// alpha -> beta where (alpha in [Vt or Vn]+) and (beta in [Vt or Vn]*) and (|alpha| <= |beta|)

	// Vt - small
	// Vn - big

	std::vector<std::string> vt = { "a", "b", "c", };
	std::vector<std::string> vn = { "B", "C", };

	grammary.setS("S");
	grammary.setVt(vt);
	grammary.setVn(vn);

	grammary.addOneP("S", "aSBC");
	grammary.addOneP("S", "abc");
	grammary.addOneP("bC", "bc");
	grammary.addOneP("CB", "BC");
	grammary.addOneP("cC", "cc");
	grammary.addOneP("BB", "bb");

	std::cout << "Expected: Type1" << std::endl;
	std::cout << "  Formal: " << grammary.alalyze() << std::endl;
	std::cout << "  Result: " << (eType1 == grammary.getType()) << std::endl;
}

void Test_Type2()
{
	CGrammary grammary;

	// test context-independent
	// A -> beta where (A in Vn) and (beta in V*)

	// Vt - small
	// Vn - big

	std::vector<std::string> vt = { "a", "b", "c", };
	std::vector<std::string> vn = { "Q", };

	grammary.setS("S");
	grammary.setVt(vt);
	grammary.setVn(vn);

	grammary.addOneP("S", "aQb");
	grammary.addOneP("S", "accb");
	grammary.addOneP("Q", "cSc");

	std::cout << "Expected: Type2" << std::endl;
	std::cout << "  Formal: " << grammary.alalyze() << std::endl;
	std::cout << "  Result: " << (eType2 == grammary.getType()) << std::endl;
}

void Test_Type3()
{
	CGrammary grammary;

	// test regular
	// (R) A -> aB|a where (a in Vt) and (A,B in Vn)
	// (L) A -> Ba|a where (a in Vt) and (A,B in Vn)

	// Vt - small
	// Vn - big

	std::vector<std::string> vt = { "a", "b", "|", };
	std::vector<std::string> vn = { "A", "B", };

	grammary.setS("S");
	grammary.setVt(vt);
	grammary.setVn(vn);

	grammary.addOneP("S", "A|");
	grammary.addOneP("S", "B|");
	grammary.addOneP("A", "a");
	grammary.addOneP("A", "Ba");
	grammary.addOneP("B", "b");
	grammary.addOneP("B", "Bb");
	grammary.addOneP("B", "Ab");

	std::cout << "Expected: Type3L" << std::endl;
	std::cout << "  Formal: " << grammary.alalyze() << std::endl;
	std::cout << "  Result: " << ((eType3R == grammary.getType()) || (eType3L == grammary.getType())) << std::endl;
}

void Test_Lab1_Variant7()
{
	CGrammary grammary;

	std::vector<std::string> vt = { "1", "0", SZ_EPSILON, };
	std::vector<std::string> vn = { "Q", };

	grammary.setS("S");
	grammary.setVt(vt);
	grammary.setVn(vn);

	grammary.addOneP("S", "Q0101");
	grammary.addOneP("Q", SZ_EPSILON);
	grammary.addOneP("Q", "10Q01");

	std::cout << "Expected: Type2" << std::endl;
	std::cout << "  Formal: " << grammary.alalyze() << std::endl;
	std::cout << "  Result: " << (eType2 == grammary.getType()) << std::endl;
}

void test()
{
	CGrammary grammary;

	std::vector<std::string> vt = { "a", };
	std::vector<std::string> vn = { "A", "B", };

	grammary.setS("S");
	grammary.setVt(vt);
	grammary.setVn(vn);

	grammary.addOneP("SA", "B");
	grammary.addOneP("B", "a");

	std::cout << "Expected: Type0" << std::endl;
	std::cout << "  Formal: " << grammary.alalyze() << std::endl;
	std::cout << "  Result: " << (eType0 == grammary.getType()) << std::endl;
}

void test1()
{
	CGrammary grammary;

	std::vector<std::string> vt = { "a", "b", SZ_EPSILON, };
	std::vector<std::string> vn = { "S", };

	//grammary.setS("S");
	grammary.setVt(vt);
	grammary.setVn(vn);

	grammary.addOneP("S", SZ_EPSILON);
	grammary.addOneP("S", "aSSb");
	grammary.addOneP("S", "bSSa");

	std::cout << "Expected: Type2" << std::endl;
	std::cout << "  Formal: " << grammary.alalyze() << std::endl;
	std::cout << "  Result: " << (eType2 == grammary.getType()) << std::endl;
}

int main(int argc, char **argv)
{
	Test_Type0();
	Test_Type1();
	Test_Type2();
	Test_Type3();
	Test_Lab1_Variant7();
	test();
	test1();

	return 0;
}
