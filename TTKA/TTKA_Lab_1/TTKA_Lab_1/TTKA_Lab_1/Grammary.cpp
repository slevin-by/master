#include "Grammary.h"

std::string CGrammary::alalyze()
{
	// add S to Vn
	if (!isExistInVn(m_S))
		m_Vn.push_back(m_S);

	// build (Vt \/ Vn)
	m_VtVn.clear();
	for (auto it : m_Vn) m_VtVn.push_back(it);
	for (auto it : m_Vt) m_VtVn.push_back(it);

	// Analyze
	m_eType = eType0;
	if (isContextDependent())
		m_eType = eType1;
	if (isContextIndependent())
		m_eType = eType2;
	if (isRegularRightAlighned())
		m_eType = eType3R;
	if (isRegularLeftAlighned())
		m_eType = eType3L;

	int type = ((int)m_eType > 3) ? 3 : m_eType;
	const char *info = g_szGrammaryType[m_eType];

	std::string result = "[" + std::to_string(type) + "] " + info;
	return result;
}

// alpha -> beta where (alpha in [Vt or Vn]+) and (beta in [Vt or Vn]*) and (|alpha| <= |beta|)
bool CGrammary::isContextDependent()
{
#define alpha it.first
#define beta it.second
	for (auto &it : m_P)
	{
		bool b1 = isExistInVnVt(alpha, false);
		bool b2 = isExistInVnVt(beta, true);
		bool b3 = (alpha.size() <= beta.size());

		if (!(b1 & b2 & b3))
			return false;
	}
	return true;
#undef alpha
#undef beta
}

// A -> beta where (A in Vn) and (beta in V*)
bool CGrammary::isContextIndependent()
{
#define A it.first
#define beta it.second
	for (auto &it : m_P)
	{
		if (!(isExistInVn(A) && isExistInVMul(beta)))
			return false;
	}
	return true;
#undef A
#undef beta
}

// A -> aB|a where (a in Vt) and (A,B in Vn)
bool CGrammary::isRegularRightAlighned()
{
#define A it.first
#define a it.second.front()
#define B it.second.back()
	for (auto &it : m_P)
	{
		if (it.first.length() > 1 || it.second.length() > 2)			////// Added to prevent two-or-more symbols
			return false;												////// input for regular grammary
		if (!(isExistInVn(A) && (isExistInVt(a) || isExistInVn(B))))
			return false;
	}
	return true;
#undef A
#undef B
#undef a
}

// A -> Ba|a where (a in Vt) and (A,B in Vn)
bool CGrammary::isRegularLeftAlighned()
{
#define A it.first
#define B it.second.front()
#define a it.second.back()
	for (auto &it : m_P)
	{
		if (it.first.length() > 1 || it.second.length() > 2)			////// Added to prevent two-or-more symbols
			return false;												////// input for regular grammary
		if (!(isExistInVn(A) && (isExistInVt(a) || isExistInVn(B))))
			return false;
	}
	return true;
#undef A
#undef B
#undef a
}

bool CGrammary::isExistInVt(std::string obj)
{
	for (char c : obj)
	{
		if (!isExistInVt(c))
			return false;
	}
	return true;
}

bool CGrammary::isExistInVt(char obj)
{
	std::string s(1, obj);
	return std::find(m_Vt.begin(), m_Vt.end(), s) != m_Vt.end();
}

bool CGrammary::isExistInVn(std::string obj)
{
	if (obj.length() > 1)						///// Added to prevent two-or-more symbols input
		return false;							///// for Context-Independent grammary
	for (char c : obj)
	{
		if (!isExistInVn(c))
			return false;
	}
	return true;
}

bool CGrammary::isExistInVn(char obj)
{
	std::string s(1, obj);
	return std::find(m_Vn.begin(), m_Vn.end(), s) != m_Vn.end();
}

bool CGrammary::isExistInVPlus(std::string obj)
{
	if (obj.find(EPSILON) != std::string::npos)
		return false;
	return true;
}

bool CGrammary::isExistInVMul(std::string obj)
{
	return true;
}

bool CGrammary::isExistInVnVt(std::string obj, bool hasEmpty)
{
	std::string s = " ";
	for (char c : obj)
	{
		if (!hasEmpty && (c == EPSILON))
			return false;

		s[0] = c;
		if (std::find(m_VtVn.begin(), m_VtVn.end(), s) == m_VtVn.end())
			return false;
	}
	return true;
}

bool CGrammary::isNotEmpty(std::string obj)
{
	if (obj.find(EPSILON) != std::string::npos)
		return true;
	return false;
}
