#ifndef _GRAMMARY_H_
#define _GRAMMARY_H_

#include <vector>
#include <string>
#include <map>

#define EPSILON ' '
#define SZ_EPSILON " "

enum EGrammaryType
{
	eType0,
	eType1,
	eType2,
	eType3R,
	eType3L,
};

const char g_szGrammaryType[][100] = {
	"Type0",
	"Context-Dependent",
	"Context-Independent",
	"Regular (right alighned)",
	"Regular (left alighned)",
};

class CGrammary
{
	using alphabeth_t = std::vector<std::string>;
	using rule_t = std::multimap<std::string, std::string>;
public:
	CGrammary() = default;

public:
	void setVt(std::vector<std::string> &Vt)
	{
		m_Vt = Vt;
	}
	void setVn(std::vector<std::string> &Vn)
	{
		m_Vn = Vn;
	}
	void addOneP(std::string l, std::string r)
	{
		m_P.emplace(l, r);
	}
	void setS(std::string S)
	{
		m_S = S;
	}

	std::string alalyze();

	int getType() const
	{
		return m_eType > 3
			? 3
			: m_eType;
	}

private:
	bool isContextDependent();
	bool isContextIndependent();
	bool isRegularRightAlighned();
	bool isRegularLeftAlighned();

	bool isExistInVt(std::string obj);
	bool isExistInVt(char obj);
	bool isExistInVn(std::string obj);
	bool isExistInVn(char obj);

	bool isExistInVPlus(std::string obj);
	bool isExistInVMul(std::string obj);

	bool isExistInVnVt(std::string obj, bool hasEmpty);

	bool isNotEmpty(std::string obj);

private:
	alphabeth_t m_Vt;
	alphabeth_t m_Vn;
	rule_t m_P;
	std::string m_S;

	EGrammaryType m_eType;

	alphabeth_t m_VtVn;
};

#endif // _GRAMMARY_H_
