#ifndef _DFA_IMPORT_H_
#define _DFA_IMPORT_H_

#include "IFsmImport.h"
#include <tuple>
#include <vector>
#include <string>

class CDfaImport : public IFsmImport
{
public:
	using func_t = std::tuple<std::string, char, std::string>;

	CDfaImport() = default;

	std::vector<std::string> Qd;	// Vn
	std::vector<char> T;	// Vt
	std::vector<func_t> Fd; // P
	char H; // S
	std::vector<std::string> Zd; // N
};

#endif // _DFA_IMPORT_H_
