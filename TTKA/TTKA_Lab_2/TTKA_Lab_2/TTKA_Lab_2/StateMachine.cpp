#include "StateMachine.h"
#include <queue>
#include <set>

void CStateMachine::importFromGrammary(std::unique_ptr<GrammaryExport> &ge)
{
	if (!ge.get())
		return;

	StateMachineImport smi = ge->convertToStatemachine();
	m_Q = smi.Q;
	m_T = smi.T;
	m_F = smi.F;
	m_H = smi.H;
	m_Z = smi.Z;
}

std::string CStateMachine::getStateNondeterministic(char input, char condition)
{
	if (!isExistInQ(input) || !isExistInT(condition))
		return "";

	std::string tmp;
	for (auto it : m_F)
	{
		if ((std::get<in>(it) == input) && (std::get<cond>(it) == condition))
			tmp.append(std::string(1, std::get<out>(it)) + ", ");
	}
	if (!tmp.empty())
		tmp.erase(tmp.begin() + tmp.size() - 2, tmp.end());
	return tmp;
}

std::string CStateMachine::getStateNondeterministicEx(char input, char condition)
{
	if (!isExistInQ(input) || !isExistInT(condition))
		return "";

	std::string tmp;
	for (auto it : m_F)
	{
		if ((std::get<in>(it) == input) && (std::get<cond>(it) == condition))
			tmp.append(std::string(1, std::get<out>(it)));
	}
	return tmp;
}

std::string CStateMachine::getStateDeterministic(std::string input, char condition)
{
	if (m_Fd.empty())
		return "";

	if (!isExistInQd(input) || !isExistInT(condition))
		return "";

	std::string tmp;
	for (auto it : m_Fd)
	{
		if ((std::get<in>(it) == input) && (std::get<cond>(it) == condition))
			return std::get<out>(it);
	}
	return "";
}

void CStateMachine::convertToDeterministic()
{
	std::queue<std::string> Q;
	std::vector<std::string> pool;

	std::string state;
	state.reserve(2);

	state = makeNewState(m_H);
	Q.push(state);
	pool.push_back(state);
	//m_Qd.push_back(state);

	for (;;)
	{
		std::string qd = Q.front();
		Q.pop();

		for (char condition : m_T)
		{
			state.clear();

			for (char input : qd)
			{
				state.append(getStateNondeterministicEx(input, condition));
			}

			// sort symbols in the state name
			if ((state.size() > 1) && (state[1] < state[0]))
				std::swap(state[0], state[1]);

			if (!state.empty())
			{
				m_Fd.push_back(func_t_ex(qd, condition, state));
				if (std::find(pool.begin(), pool.end(), state) == pool.end())
				{
					Q.push(state);
					pool.push_back(state);
				}
			}
			//if (!state.empty() && (std::find(pool.begin(), pool.end(), state) == pool.end()))
			//{
			//	Q.push(state);
			//	pool.push_back(state);

			//	//if (state.find(m_Z) != std::string::npos)
			//	//	m_Qd.push_back(state);
			//}
		}

		if (Q.empty())
			break;
	}

	m_Qd = pool;

	for (auto it : pool)
	{
		if (it.find(m_Z) != std::string::npos)
			m_Zd.push_back(it);
	}

//	printf("%d\n", m_Zd.size());
//	for (auto s : m_Zd)
//		printf("%s\n", s.c_str());

//	printf("%d\n", pool.size());
//	for (auto s : pool)
//		printf("%s\n", s.c_str());

//	printf("%d\n", m_Fd.size());
//	for (auto f : m_Fd)
//		printf("%s(%c)=%s\n", std::get<in>(f).c_str(), std::get<cond>(f), std::get<out>(f).c_str());
}

size_t CStateMachine::stateNumber() const
{
	return m_Q.size() + 2;
}

size_t CStateMachine::conditionNumber() const
{
	return m_T.size();
}

bool CStateMachine::isExistInQ(char obj) const
{
	return std::find(m_Q.cbegin(), m_Q.cend(), obj) != m_Q.cend();
}

bool CStateMachine::isExistInQd(std::string obj) const
{
	return std::find(m_Qd.cbegin(), m_Qd.cend(), obj) != m_Qd.cend();
}

bool CStateMachine::isExistInT(char obj) const
{
	return std::find(m_T.cbegin(), m_T.cend(), obj) != m_T.cend();
}

std::string CStateMachine::makeNewState(char c)
{
	return std::string(1, c);
}

std::string CStateMachine::makeNewState(char c1, char c2)
{
	std::string state;
	state.reserve(2);
	state[0] = c1;
	state[1] = c2;
	return state;
}
