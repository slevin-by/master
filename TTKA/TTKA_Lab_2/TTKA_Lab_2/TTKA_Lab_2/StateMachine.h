#ifndef _STATE_MACHINE_H_
#define _STATE_MACHINE_H_

#include "GrammaryExport.h"
//#include <iostream>
#include <memory>

#include "IFSM.h"

/*enum
{
	in,
	cond,
	out,
};*/

class CStateMachine
{
	using func_t = std::tuple<char, char, char>;
	using func_t_ex = std::tuple<std::string, char, std::string>;

public:
	CStateMachine() = default;

	void importFromGrammary(std::unique_ptr<GrammaryExport> &ge);

	std::string getStateNondeterministic(char input, char condition);
	std::string getStateNondeterministicEx(char input, char condition);
	std::string getStateDeterministic(std::string input, char condition);

	void convertToDeterministic();

	size_t stateNumber() const;
	size_t conditionNumber() const;

private:
	bool isExistInQ(char obj) const;
	bool isExistInQd(std::string obj) const;
	bool isExistInT(char obj) const;

	std::string makeNewState(char c);
	std::string makeNewState(char c1, char c2);

private:
	std::vector<char> m_Q;
	std::vector<char> m_T;
	std::vector<func_t> m_F;
	char m_H;
	char m_Z;

	std::vector<std::string> m_Qd;
	std::vector<func_t_ex> m_Fd;
	std::vector<std::string> m_Zd;
};

#endif // _STATE_MACHINE_H_
