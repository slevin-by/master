#ifndef _DFA_H_
#define _DFA_H_

#include "IFSM.h"

class CDFA : public IFSM
{
	using func_t = std::tuple<std::string, char, std::string>;

public:
	CDFA() {}

	bool import(std::shared_ptr<IFsmImport> &rFsmi);

	std::string getNextState(std::string input, char condition);
	char getNextState(char input, char condition);

	std::vector<char> getUpdatedInputs();
	std::vector<char> getUpdatedFinish();

private:
	bool isExistInQ(char obj) const;
	bool isExistInT(char obj) const;

	bool isExistInQd(std::string obj) const;

private:
	std::vector<std::string> m_Qd;
	std::vector<char> m_T;
	std::vector<func_t> m_Fd;
	char m_H;
	std::vector<std::string> m_Zd;

	std::map<std::string, char> m_mapNameSz;
	std::map<char, std::string> m_mapNameCh;
	std::map<char, int> m_mapWeight;
};

#endif // _DFA_H_
