import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import pylab

G = nx.DiGraph()

def parseFile():
    file_name = 'serial.txt'
    f = open(file_name, 'r')
    for line in f:
        line = line.strip()
        func = line.split(':', 2)
        G.add_edges_from(eval(func[0]), weight=int(func[1]))

def main():
    parseFile()

    edge_labels=dict([((u,v,),d['weight'])
                 for u,v,d in G.edges(data=True)])
    edge_colors = ['black' for edge in G.edges()]

    #pos=nx.spring_layout(G)
    #pos=nx.shell_layout(G)
    #pos=nx.random_layout(G)
    pos=nx.circular_layout(G)
    node_labels = {node:node for node in G.nodes()}
    nx.draw_networkx_labels(G, pos, labels=node_labels)
    nx.draw_networkx_edge_labels(G,pos,edge_labels=edge_labels)
    #nx.draw(G,pos, node_color = values, node_size=1500,edge_color=edge_colors,edge_cmap=plt.cm.Reds)
    nx.draw(G,pos, node_size=1500, edge_color=edge_colors)
    pylab.show()

if __name__ == '__main__':
    main()
