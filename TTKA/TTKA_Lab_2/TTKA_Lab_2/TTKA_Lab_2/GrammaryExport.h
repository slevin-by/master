#ifndef _GRAMMARY_EXPORT_H_
#define _GRAMMARY_EXPORT_H_

#include <vector>
#include <map>
#include <tuple>
#include <memory>
#include <queue>
#include "NFA.h"
#include "DFA.h"
#include "NfaImport.h"
#include "DfaImport.h"

struct StateMachineImport
{
	using func_t = std::tuple<char, char, char>;

	std::vector<char> Q;	// Vn
	std::vector<char> T;	// Vt
	std::vector<func_t> F; // P
	char H; // S
	char Z; // N
};

struct GrammaryExport
{
	char S;
	std::vector<char> Vn;
	std::vector<char> Vt;
	std::multimap<char, std::string> P;

	StateMachineImport convertToStatemachine()
	{
		StateMachineImport smi;

		smi.Q = Vn;
		smi.T = Vt;
		smi.H = S;
		smi.Z = (std::find(Vn.begin(), Vn.end(), 'N') == Vn.end())
			? 'N'
			: '\0';

		StateMachineImport::func_t func;
		for (auto itm : P)
		{
			if (itm.second.size() < 2) // Edit Rule
			{
				std::string tmp = itm.second + smi.Z;
				itm.second.swap(tmp);
			}

			func = StateMachineImport::func_t(
				itm.first, itm.second.front(), itm.second.back());
			smi.F.push_back(func);
		}

		return smi;
	}

	std::shared_ptr<IFsmImport> convertToStatemachineEx()
	{
		std::shared_ptr<IFsmImport> _smi(new CNfaImport());
		CNfaImport *smi = dynamic_cast<CNfaImport *>(_smi.get());

		smi->Q = Vn;
		smi->T = Vt;
		smi->H = S;
		smi->Z = (std::find(Vn.begin(), Vn.end(), 'N') == Vn.end())
			? 'N'
			: '\0';

		CNfaImport::func_t func;
		for (auto itm : P)
		{
			if (itm.second.size() < 2) // Edit Rule
			{
				std::string tmp = itm.second + smi->Z;
				itm.second.swap(tmp);
			}

			func = StateMachineImport::func_t(
				itm.first, itm.second.front(), itm.second.back());
			smi->F.push_back(func);
		}

		return _smi;
	}
};

enum EFactory
{
	eFactoryNfa,
	eFactoryDfa,
};

class Fsmfactory
{
	std::shared_ptr<GrammaryExport> m_export;
public:
	void init(std::shared_ptr<GrammaryExport> &ge)
	{
		m_export.swap(ge);
	}

	std::shared_ptr<IFSM> create(EFactory type)
	{
		if (!m_export)
			return nullptr;

		IFSM *fsm = new CNFA();
		if (fsm)
			fsm->import(m_export->convertToStatemachineEx());

		if (type == eFactoryDfa)
		{
			IFSM *tmpDfa = new CDFA();
			tmpDfa->import(dynamic_cast<CNFA *>(fsm)->toDFA());
			delete fsm;
			fsm = tmpDfa;
		}

		std::shared_ptr<IFSM> _fsm(fsm);
		return _fsm;
	}

	static std::shared_ptr<IFSM> create(std::shared_ptr<GrammaryExport> &ge, EFactory type)
	{
		IFSM *fsm = nullptr;
		switch (type)
		{
		case eFactoryNfa:
			fsm = new CNFA();
			break;
		case eFactoryDfa:
			fsm = new CDFA();
			break;
		default:
			break;
		}

		if (fsm)
			fsm->import(ge->convertToStatemachineEx());

		std::shared_ptr<IFSM> _fsm(fsm);
		return _fsm;
	}

private:
	/*void NfaToDfa(std::shared_ptr<IFsmImport> &rsmi)
	{
		CNfaImport *smi = dynamic_cast<CNfaImport *>(rsmi.get());

		std::queue<std::string> Q;
		std::vector<std::string> pool;

		std::string state;
		state.reserve(2);

		state = makeNewState(smi->H);
		Q.push(state);
		pool.push_back(state);

		for (;;)
		{
			std::string qd = Q.front();
			Q.pop();

			for (char condition : smi->T)
			{
				state.clear();

				for (char input : qd)
				{
					state.append(getStateNondeterministicEx(input, condition));
				}

				// sort symbols in the state name
				if ((state.size() > 1) && (state[1] < state[0]))
					std::swap(state[0], state[1]);

				if (!state.empty())
				{
					m_Fd.push_back(func_t_ex(qd, condition, state));
					if (std::find(pool.begin(), pool.end(), state) == pool.end())
					{
						Q.push(state);
						pool.push_back(state);
					}
				}
				//if (!state.empty() && (std::find(pool.begin(), pool.end(), state) == pool.end()))
				//{
				//	Q.push(state);
				//	pool.push_back(state);

				//	//if (state.find(m_Z) != std::string::npos)
				//	//	m_Qd.push_back(state);
				//}
			}

			if (Q.empty())
				break;
		}

		m_Qd = pool;

		for (auto it : pool)
		{
			if (it.find(m_Z) != std::string::npos)
				m_Zd.push_back(it);
		}

		//	printf("%d\n", m_Zd.size());
		//	for (auto s : m_Zd)
		//		printf("%s\n", s.c_str());

		//	printf("%d\n", pool.size());
		//	for (auto s : pool)
		//		printf("%s\n", s.c_str());

		//	printf("%d\n", m_Fd.size());
		//	for (auto f : m_Fd)
		//		printf("%s(%c)=%s\n", std::get<in>(f).c_str(), std::get<cond>(f), std::get<out>(f).c_str());
	}

	std::string makeNewState(char c)
	{
		return std::string(1, c);
	}

	std::string makeNewState(char c1, char c2)
	{
		std::string state;
		state.reserve(2);
		state[0] = c1;
		state[1] = c2;
		return state;
	}*/
};

#endif // _GRAMMARY_EXPORT_H_
