#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
#include <fstream>
#include "Grammary.h"
#include "StateMachine.h"

#include "GrammaryExport.h"

void test1()
{
	CGrammary grammary;

	std::vector<std::string> Vt = { "a", "b", };
	std::vector<std::string> Vn = { "A", "B", };

	grammary.setS("S");
	grammary.setVt(Vt);
	grammary.setVn(Vn);

	grammary.addOneP("S", "aB"); // 1
	grammary.addOneP("S", "aA"); // 1
	grammary.addOneP("B", "bB"); // 2
	grammary.addOneP("B", "a");  // 2
	grammary.addOneP("A", "aA"); // 3
	grammary.addOneP("A", "b");  // 3

	std::cout << grammary.alalyze() << std::endl;

	std::shared_ptr<GrammaryExport> spGe = grammary.exportGrammaryEx();

	std::shared_ptr<IFSM> spNfa = Fsmfactory::create(spGe, eFactoryNfa);
}

void test()
{
	CGrammary grammary;

	std::vector<std::string> Vt = { "a", "b", };
	std::vector<std::string> Vn = { "A", "B", };

	grammary.setS("S");
	grammary.setVt(Vt);
	grammary.setVn(Vn);

	grammary.addOneP("S", "aB"); // 1
	grammary.addOneP("S", "aA"); // 1
	grammary.addOneP("B", "bB"); // 2
	grammary.addOneP("B", "a");  // 2
	grammary.addOneP("A", "aA"); // 3
	grammary.addOneP("A", "b");  // 3

	std::cout << grammary.alalyze() << std::endl;

	std::unique_ptr<GrammaryExport> grmExp = grammary.exportGrammary();

	CStateMachine sm;
	sm.importFromGrammary(grmExp);
	sm.convertToDeterministic();
}

CGrammary BuildGrammary()
{
	CGrammary grammary;

	std::vector<std::string> Vt = { "0", "1", "x", "y", "z", };
	std::vector<std::string> Vn = { "Y", "Z", "V", "W", };

	grammary.setS("X");
	grammary.setVt(Vt);
	grammary.setVn(Vn);

	grammary.addOneP("X", "yY"); // 1
	grammary.addOneP("X", "zZ"); // 1
	grammary.addOneP("Y", "1V"); // 2
	grammary.addOneP("Z", "0W"); // 3
	grammary.addOneP("Z", "0Y"); // 3
	grammary.addOneP("V", "xZ"); // 4
	grammary.addOneP("V", "xW"); // 4
	grammary.addOneP("V", "1");  // 4
	grammary.addOneP("W", "1Y"); // 5
	grammary.addOneP("W", "0");  // 5

	return std::move(grammary);
}

void Var7()
{
	CGrammary grammary;

	std::vector<std::string> Vt = { "0", "1", "x", "y", "z", };
	std::vector<std::string> Vn = { "Y", "Z", "V", "W", };

	grammary.setS("X");
	grammary.setVt(Vt);
	grammary.setVn(Vn);

	grammary.addOneP("X", "yY"); // 1
	grammary.addOneP("X", "zZ"); // 1
	grammary.addOneP("Y", "1V"); // 2
	grammary.addOneP("Z", "0W"); // 3
	grammary.addOneP("Z", "0Y"); // 3
	grammary.addOneP("V", "xZ"); // 4
	grammary.addOneP("V", "xW"); // 4
	grammary.addOneP("V", "1");  // 4
	grammary.addOneP("W", "1Y"); // 5
	grammary.addOneP("W", "0");  // 5

	std::cout << grammary.alalyze() << std::endl;

	std::shared_ptr<GrammaryExport> spGe = grammary.exportGrammaryEx();

	Fsmfactory factory;
	factory.init(spGe);

	std::shared_ptr<IFSM> spDfa = factory.create(eFactoryDfa);
	std::vector<char> inputs = dynamic_cast<CDFA *>(spDfa.get())->getUpdatedInputs();
	std::vector<char> finish = dynamic_cast<CDFA *>(spDfa.get())->getUpdatedFinish();

	std::ofstream ofs("serial.txt", std::ofstream::out | std::ofstream::trunc);
	if (!ofs.is_open())
		return;

	for (char c : inputs)
	{
		for (auto szW : Vt)
		{
			char out = spDfa->getNextState(c, szW[0]);
			if (out != '\0')
				ofs << "[('" << c << "','" << out << "')]"
					<< ":" << (int)(szW[0] - '0') << std::endl;
		}
	}

	ofs.close();
}

int main(int argc, char **argv)
{
	//test();
	//return 0;

	Var7();
	return 0;

	CGrammary grammary = BuildGrammary();
	std::cout << grammary.alalyze() << std::endl;

	std::unique_ptr<GrammaryExport> grmExp = grammary.exportGrammary();

	CStateMachine sm;
	sm.importFromGrammary(grmExp);

	const size_t cellSize = 80 / sm.stateNumber();
	const size_t nullSize = 80 - (cellSize * sm.stateNumber());

	std::string tmp;
	tmp.reserve(cellSize);

	std::string szTable;
	szTable.reserve(1024);

	// Draw header
	for (int i = 0; i < cellSize - 1; i++)
		szTable += " ";
	szTable += "F";
	for (auto it : grmExp->Vn)
	{
		for (int i = 0; i < cellSize - 1; i++)
			szTable += " ";
		szTable += it;
	}
	for (int i = 0; i < cellSize - 1; i++)
		szTable += " ";
	szTable += "N";
	for (int i = 0; i < nullSize; i++)
		szTable += " ";

	// Draw lines
	std::vector<char> header = grmExp->Vn;
	header.push_back('N');
	for (auto cond : grmExp->Vt)
	{
		// Draw vertical
		for (int i = 0; i < cellSize - 1; i++)
			szTable += " ";
		szTable += std::string(1, cond);

//		std::cout << szTable << std::endl;

		// Draw line
		for (auto input : header)
		{
			tmp = sm.getStateNondeterministic(input, cond);
			for (int i = 0; i < cellSize - tmp.size(); i++)
				szTable += " ";
			szTable += tmp;
		}

		for (int i = 0; i < nullSize; i++)
			szTable += " ";
	}

	std::cout << szTable << std::endl << std::endl << std::endl;


	//std::cout << sm.getStateNondeterministic('V', 'x') << std::endl;


	sm.convertToDeterministic();

	tmp = "";
	szTable = "";

	// Draw header
	for (int i = 0; i < cellSize - 1; i++)
		szTable += " ";
	szTable += "F";
	for (auto it : grmExp->Vn)
	{
		for (int i = 0; i < cellSize - 1; i++)
			szTable += " ";
		szTable += it;
	}
	for (int i = 0; i < cellSize - 1; i++)
		szTable += " ";
	szTable += "N";
	for (int i = 0; i < nullSize; i++)
		szTable += " ";

	// Draw lines
	header = grmExp->Vn;
	header.push_back('N');
	for (auto cond : grmExp->Vt)
	{
		// Draw vertical
		for (int i = 0; i < cellSize - 1; i++)
			szTable += " ";
		szTable += std::string(1, cond);

		//		std::cout << szTable << std::endl;

		// Draw line
		for (auto input : header)
		{
			tmp = sm.getStateNondeterministicEx(input, cond);
			for (int i = 0; i < cellSize - tmp.size(); i++)
				szTable += " ";
			szTable += tmp;
		}

		for (int i = 0; i < nullSize; i++)
			szTable += " ";
	}

	std::cout << szTable << std::endl << std::endl << std::endl;

	return 0;
}
