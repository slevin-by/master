#include "NFA.h"
#include "NfaImport.h"

#include "DFA.h"
#include <queue>
#include "DfaImport.h"

bool CNFA::import(std::shared_ptr<IFsmImport> &rFsmi)
{
	if (!rFsmi.get())
		return false;

	CNfaImport *smi = dynamic_cast<CNfaImport *>(rFsmi.get());
	m_Q = smi->Q;
	m_T = smi->T;
	m_F = smi->F;
	m_H = smi->H;
	m_Z = smi->Z;

	return true;
}

std::string CNFA::getNextState(std::string input, char condition)
{
	if (!isExistInQ(input[0]) || !isExistInT(condition))
		return "";

	std::string tmp;
	for (auto it : m_F)
	{
		if ((std::get<in>(it) == input[0]) && (std::get<cond>(it) == condition))
			tmp.append(std::string(1, std::get<out>(it)));
	}
	return tmp;
}

char CNFA::getNextState(char input, char condition)
{
	if (!isExistInQ(input) || !isExistInT(condition))
		return '\0';

	std::string tmp;
	for (auto it : m_F)
	{
		if ((std::get<in>(it) == input) && (std::get<cond>(it) == condition))
			return std::get<out>(it);
	}
	return '\0';
}

std::shared_ptr<IFsmImport> CNFA::toDFA()
{	
	std::shared_ptr<IFsmImport> _dfaImport(new CDfaImport());
	CDfaImport *dfaImport = dynamic_cast<CDfaImport *>(_dfaImport.get());
	dfaImport->T = m_T;
	dfaImport->H = m_H;

	auto makeNewState = [](char c)
	{
		return std::string(1, c);
	};

	std::queue<std::string> Q;
	std::vector<std::string> pool;

	std::string state;
	state.reserve(2);

	std::string szInput;
	szInput.reserve(2);

	state = makeNewState(m_H);
	Q.push(state);
	pool.push_back(state);
	//m_Qd.push_back(state);

	for (;;)
	{
		std::string qd = Q.front();
		Q.pop();

		for (char condition : m_T)
		{
			state.clear();

			for (char input : qd)
			{
				szInput = input;
				state.append(getNextState(szInput, condition));
			}

			// sort symbols in the state name
			if ((state.size() > 1) && (state[1] < state[0]))
				std::swap(state[0], state[1]);

			if (!state.empty())
			{
				dfaImport->Fd.push_back(CDfaImport::func_t(qd, condition, state));
				if (std::find(pool.begin(), pool.end(), state) == pool.end())
				{
					Q.push(state);
					pool.push_back(state);
				}
			}
		}

		if (Q.empty())
			break;
	}

	dfaImport->Qd = pool;

	for (auto it : pool)
	{
		if (it.find(m_Z) != std::string::npos)
			dfaImport->Zd.push_back(it);
	}

	return _dfaImport;
}

bool CNFA::isExistInQ(char obj) const
{
	return std::find(m_Q.cbegin(), m_Q.cend(), obj) != m_Q.cend();
}

bool CNFA::isExistInT(char obj) const
{
	return std::find(m_T.cbegin(), m_T.cend(), obj) != m_T.cend();
}
