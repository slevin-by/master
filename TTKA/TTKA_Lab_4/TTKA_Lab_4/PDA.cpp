#include "PDA.h"
#include <set>

CPDA::CPDA()
{
}

CPDA::CPDA(PbaImportBottomUp &pbai)
{
	import(pbai);
}

CPDA::CPDA(PbaImportTopDown &pbai)
{
	for (auto itv : pbai.F)
		m_mmF.insert(std::make_pair(itv.first, itv.second));

	m_q0 = pbai.q0;
	m_N0 = pbai.N0;

	m_Vt = pbai.Vt;
	m_Vn = pbai.Vn;

	m_szStack = "";
}

CPDA::~CPDA()
{
}

void CPDA::import(PbaImportBottomUp &pbai)
{	
	for (auto itv : pbai.F)
		m_F.insert(std::make_pair(itv.first, itv.second));

	m_q0 = pbai.q0;
	m_N0 = pbai.N0;

	m_szStack = "";

	// Sort rules.
	// The long rules must be at the begginning.
	std::multiset <std::pair<size_t, std::pair<input_t, output_t>>> funcSortSet;
	for (auto &itm : m_F)
	{
		size_t len = std::get<2>(itm.first).size();
		input_t i = itm.first;
		output_t o = itm.second;
		funcSortSet.emplace(len, std::make_pair(i, o));
	}

 	m_vF.clear();
	for (decltype(funcSortSet)::reverse_iterator rits = funcSortSet.rbegin(); rits != funcSortSet.rend(); ++rits)
		m_vF.push_back(std::make_pair(std::get<2>(rits->second.first), rits->second.second.second));
}

void CPDA::parseBottomUp(std::string str)
{
	int i = 0;

	for (;;)
	{
		m_szStack = str[i] + m_szStack;
		m_N0 = str[i];

		printf("[Input] %c\n", m_N0);

		input_t input;
		if (m_szStack.length() >= 2)
		{
			for (auto &itm : m_vF)
			{
				std::string szInput = itm.first;
				size_t nLength = szInput.length();

				char *szStackBatch = (char *)calloc(nLength + 1, sizeof(char));
				if (nLength <= m_szStack.length())
					m_szStack.copy(szStackBatch, nLength);
				else
				{
					free(szStackBatch);
					continue;
				}

				if (szInput == szStackBatch)
				{
					input = input_t(m_q0, EPSILON, szStackBatch);
					free(szStackBatch);
					goto L_CheckInput;
				}

				free(szStackBatch);
			}
		}

		/*input_t*/ input = input_t(m_q0, EPSILON, std::string(1, m_N0));
	L_CheckInput:
		input_t lastInput = input;
		if (m_F.find(input) != m_F.end())
		{
			while (m_F.find(input) != m_F.end())
			{

				printf("Memory: %s\n", m_szStack.c_str());
				m_N0 = m_F[input].second;

				char c;
				//Pop(c);
				m_szStack.erase(m_szStack.begin(), m_szStack.begin() + std::get<2>(input).length());

				//Push(m_N0);
				m_szStack = m_N0 + m_szStack;

				//if (m_szStack.length() < 2)
				//{
					input = input_t(m_q0, EPSILON, std::string(1, m_N0));
				//	continue;
				//}

				/*input_t lastInput = input;
				for (auto &itm : m_vF)
				{
					std::string szInput = itm.first;
					size_t nLength = szInput.length();

					char *szStackBatch = (char *)calloc(nLength + 1, sizeof(char));
					if (nLength <= m_szStack.length())
						m_szStack.copy(szStackBatch, nLength);
					else
						continue;

					if (szInput == szStackBatch)
					{
						input = input_t(m_q0, EPSILON, szStackBatch);
						break;
					}
				}*/
				
				if (lastInput == input)
					break;
				else
					lastInput = input;
			}
		}
		else
		{
		}

		printf("Memory: %s\n\n", m_szStack.c_str());
		i++;
	}
}

void CPDA::parseBottomUpEx(std::string str)
{
	int i = 0;

	for (;;)
	{
		m_szStack = str[i] + m_szStack;
		m_N0 = str[i];

		if (m_N0 == '\0' || m_szStack == "")
		{
			printf("[Input] f\n");
			printf("Memory: [END]\n");
			return;
		}

		printf("[Input] %c\n", m_N0);

		input_t input;
		
		do
		{
			printf("Memory: %s\n", m_szStack.c_str());

			for (auto &itm : m_vF)
			{
				std::string szInput = itm.first;
				size_t nLength = szInput.length();

				char *szStackBatch = (char *)calloc(nLength + 1, sizeof(char));
				if (nLength <= m_szStack.length())
					m_szStack.copy(szStackBatch, nLength);
				else
				{
					free(szStackBatch);
					continue;
				}

				if (szInput == szStackBatch)
				{
					input = input_t(m_q0, EPSILON, szStackBatch);
					free(szStackBatch);
					goto L_CheckInput;
				}

				free(szStackBatch);
			}
			goto L_next;

		L_CheckInput:
			m_N0 = m_F[input].second;
			m_szStack.erase(m_szStack.begin(), m_szStack.begin() + std::get<2>(input).length());
			m_szStack = m_N0 + m_szStack;

			input = input_t(m_q0, EPSILON, std::string(1, m_N0));
		} 
		while (m_F.find(input) != m_F.end());

		printf("Memory: %s\n", m_szStack.c_str());
	L_next:
		printf("\n");
		i++;
	}
}

void CPDA::parseTopDown(std::string a_str)
{
	int i = 0;
	char cCurr, cNext;
	std::string str = "S" + a_str;
	std::string N0 = "S";

	m_szStack.clear();

	for (;;)
	{
		cCurr = str[i];

		if (cCurr == '\0')
		{
			printf("[Input] f\n");
			printf("Memory: [END]\n");
			return;
		}
		else
		{
			cNext = str[i + 1];
			m_szStack = cCurr + m_szStack;
		}

		printf("[Input] %c\n", cCurr);

		input_t input;

		do
		{
		L_checkProcessTerm:
			auto values = m_mmF.equal_range(cCurr);
			for (int j = i + 1; j < str.size(); j++)
			{
				for (std::multimap<char, std::string>::iterator it = values.first; it != values.second; ++it)
				{
					if (it->second.find(str[j]) != std::string::npos)
					{
						m_szStack.erase(m_szStack.begin(), m_szStack.begin() + N0.length());
						N0 = it->second;
						m_szStack = N0 + m_szStack;
						goto L_checkTerm;
					}
				}
			}

		L_checkTerm:
			if (std::find(m_Vn.begin(), m_Vn.end(), m_szStack[0]) != m_Vn.end())
				goto L_checkProcessTerm;
		}
		while (m_F.find(input) != m_F.end());

		printf("Memory: %s\n", m_szStack.c_str());
	L_next:
		printf("\n");
		i++;
	}
}

std::string CPDA::findTerminal(char nt)
{
	auto values = m_mmF.equal_range(nt);
	for (std::multimap<char, std::string>::iterator it = values.first; it != values.second; ++it)
	{

	}
	return "";
}

void CPDA::bruteForce()
{
	std::map<std::string, std::vector<std::string>> vSeq;

	std::vector<std::string> vOrigins;

	auto values = m_mmF.equal_range('S');
	for (std::multimap<char, std::string>::iterator it = values.first; it != values.second; ++it)
		vOrigins.push_back(it->second);
}

void CPDA::parseTopDownEx(std::string a_str)
{
	int i = 0;
	std::string str = a_str;
	std::string N0 = "S";
	char cN0 = 'S';

	bool bNextCharFlag = false;

	m_szStack.clear();
	m_szStack = N0 + m_szStack;

	std::vector<std::string> vStacks;
	std::vector<std::string> vPrevious;

	// "Open" S the first time to run parsing
	auto values = m_mmF.equal_range(cN0);
	for (std::multimap<char, std::string>::iterator it = values.first; it != values.second; ++it)
	{
		vStacks.push_back(it->second);
		vPrevious.push_back("");
	}

	for (;;)
	{
		if (str[i] == '\0')
		{
			printf("[Input] f\n");
			printf("Memory: [END]\n");
			return;
		}

		printf("[Input] %c\n", str[i]);

		int nCurrStackSize = vStacks.size();
		for (int iStack = 0; iStack < nCurrStackSize; iStack++)
		{
			std::string szStack = vStacks[iStack];
			if (!isTerminal(szStack[0]) && (szStack[0] != vPrevious[iStack][0]))
			{
				auto values = m_mmF.equal_range(szStack[0]);
				for (std::multimap<char, std::string>::iterator it = values.first; it != values.second; ++it)
				{
					if (it->second != szStack)
					{
						std::string tmpStack = szStack;
						tmpStack.erase(tmpStack.begin(), tmpStack.begin() + 1);
						tmpStack = it->second + tmpStack;
						vStacks.push_back(tmpStack);
						vPrevious.push_back(szStack);
					}
				}
			}
			else
			{
				if (szStack[0] == str[i])
				{
					bNextCharFlag = true;
					vStacks.push_back(szStack);
					vPrevious.push_back(szStack);
				}
				else
					continue;
			}
		}

		if (nCurrStackSize != vStacks.size())
		{
			vStacks.erase(vStacks.begin(), vStacks.begin() + nCurrStackSize);
			vPrevious.erase(vPrevious.begin(), vPrevious.begin() + nCurrStackSize);

		}

		if (!bNextCharFlag)
			continue;
		
	L_next:

		std::vector<std::string> vTmpStacks;
		std::vector<std::string> vTmpPrevious;
		for (int iStack = 0; iStack < vStacks.size(); iStack++)
		{
			if (vStacks[iStack][0] == str[i])
			{
				vTmpStacks.push_back(vStacks[iStack]);
				vTmpPrevious.push_back(vPrevious[iStack]);
			}
		}
		vStacks.swap(vTmpStacks);
		vPrevious.swap(vTmpPrevious);

		vPrevious = vStacks;
		for (auto &stack : vStacks)
			stack.erase(stack.begin(), stack.begin() + 1);
		//for (auto &prev : vPrevious)
		//	prev.erase(prev.begin(), prev.begin() + 1);

		// Workaround: For correct handling recursive "S"
		nCurrStackSize = vStacks.size();
		for (int iS = 0; iS < nCurrStackSize; iS++)
		{
			if (vStacks[iS][0] == 'S')
			{
				auto values = m_mmF.equal_range(cN0);
				for (std::multimap<char, std::string>::iterator it = values.first; it != values.second; ++it)
				{
					std::string tmp = vStacks[iS];
					tmp.erase(tmp.begin(), tmp.begin() + 1);
					tmp = it->second + tmp;
					vStacks.push_back(tmp);
					vPrevious.push_back(vPrevious[iS]);
				}
			}
		}
		if (vStacks.size() > nCurrStackSize)
		{
			vStacks.erase(vStacks.begin(), vStacks.begin() + nCurrStackSize);
			vPrevious.erase(vPrevious.begin(), vPrevious.begin() + nCurrStackSize);
		}

		for (int iS = 0; iS < vStacks.size(); iS++)
			printf("[%d] Memory: %s\n", iS, vStacks[iS].c_str());

		i++;
		bNextCharFlag = false;
		printf("\n");
	}

L_exitFailure:
	printf("[ERROR] Sequence \"%s\" does not belong to the grammary.\n", a_str.c_str());
}

//void CPDA::parseTopDownEx(std::string a_str)
//{
//	//bruteForce();
//	//return;
//
//	int i = 0;
//	char cCurrInput, cNext;
//	std::string str = a_str;
//	std::string N0 = "S";
//	char cN0 = 'S';
//
//	bool bNextCharFlag = false;
//
//	m_szStack.clear();
//	m_szStack = N0 + m_szStack;
//
//	std::vector<std::string> vStacks;
//
//	std::map<int, int> mapStackChar; // key - index in stack; value - corresponding index of input string
//
//	cCurrInput = str[i];
//
//	for (;;)
//	{
//		cN0 = N0[0];
//		
//		if (cCurrInput == '\0')
//		{
//			printf("[Input] f\n");
//			printf("Memory: [END]\n");
//			return;
//		}
//		else
//		{
//			//m_szStack = cCurrInput + m_szStack;
//		}
//
//		printf("[Input] %c\n", cCurrInput);
//
//		input_t input;
//		std::string tmp;
//
//		if (!isTerminal(cN0)) // then find suitable value
//		{
//			auto values = m_mmF.equal_range(cN0);
//			for (std::multimap<char, std::string>::iterator it = values.first; it != values.second; ++it)
//				vStacks.push_back(it->second);
//
//			std::set<int> vDeleters;
//			std::vector<int> vNexters; // indexes where flow is right
//
//			int nCurrSize = vStacks.size();
//			for (int iStack = 0; iStack < nCurrSize; iStack++)
//			{
//				printf("[%d] Memory: %s\n", iStack, vStacks[iStack].c_str());
//
//				if (vStacks[iStack][0] == '\0')
//				{
//					if (str[i + 1] != '\0')
//						vDeleters.insert(iStack);
//				}
//
//				if (vStacks[iStack][0] == a_str[mapStackChar[iStack]]) // if the flow is right
//				{
//					if (a_str[mapStackChar[iStack]] == '\0')
//					{
//						printf("SUCCESS\n");
//						return;
//					}
//
//					vStacks[iStack].erase(vStacks[iStack].begin(), vStacks[iStack].begin() + 1);
//					//vNexters.push_back(iStack);
//					++mapStackChar[iStack];
//					continue;
//				}
//				else if (vStacks[iStack][0] == cN0) // if recursive rule then check next one
//				{
//					auto values = m_mmF.equal_range(vStacks[iStack][0]);
//					for (std::multimap<char, std::string>::iterator it = values.first; it != values.second; ++it)
//					{
//						if (strncmp(it->second.c_str(), vStacks[iStack].c_str(), it->second.length()))
//						{
//							std::string tmpStack = vStacks[iStack];
//							tmpStack.erase(tmpStack.begin(), tmpStack.begin() + 1);
//							tmpStack = it->second + tmpStack;
//							vStacks.push_back(tmpStack);
//							vDeleters.insert(iStack);
//						}
//					}
//				}
//				else if (!isTerminal(vStacks[iStack][0])) // if other non-terminal then reveal it
//				{
//					tmp = vStacks[iStack];
//
//					auto values = m_mmF.equal_range(vStacks[iStack][0]);
//					for (std::multimap<char, std::string>::iterator it = values.first; it != values.second; ++it)
//					{
//						if (strncmp(it->second.c_str(), vStacks[iStack].c_str(), it->second.length()))
//						{
//							std::string tmpStack = vStacks[iStack];
//							tmpStack.erase(tmpStack.begin(), tmpStack.begin() + 1);
//							tmpStack = it->second + tmpStack;
//							vStacks.push_back(tmpStack);
//							vDeleters.insert(iStack);
//						}
//						/*if (isTerminal(_it->second[0]))
//						{
//							tmp.erase(tmp.begin(), tmp.begin() + 1);
//							tmp = _it->second + tmp;
//
//							m_szStack.erase(m_szStack.begin(), m_szStack.begin() + 1);
//							m_szStack = tmp + m_szStack;
//							goto L_next;
//						}*/
//					}
//				}
//				else if (isTerminal(vStacks[iStack][0]) && (vStacks[iStack][0] != cCurrInput))
//					vDeleters.insert(iStack);
//			}
//
//			std::map<int, std::string> mapRangeStacks;
//			for (int iStack = 0; iStack < vStacks.size(); iStack++)
//				mapRangeStacks.insert(std::make_pair(iStack, vStacks[iStack]));
//			for (int iDel : vDeleters)
//			{
//				mapRangeStacks.erase(iDel);
//				mapStackChar.erase(iDel);
//			}
//			std::vector<int> vMapStackCharValues;
//			for (auto &itm : mapStackChar)
//				vMapStackCharValues.push_back(itm.second);
//			mapStackChar.clear();
//			for (int iv = 0; iv < vMapStackCharValues.size(); iv++)
//				mapStackChar[iv] = vMapStackCharValues[iv];
//			vStacks.clear();
//			for (auto &itm : mapRangeStacks)
//				vStacks.push_back(itm.second);
//
//			/*if (!vNexters.empty())
//			{
//				std::vector<std::string> vNewStacks;
//				for (int iNexter : vNexters)
//					vNewStacks.push_back(vStacks[iNexter]);
//				vStacks.swap(vNewStacks);
//				vNexters.clear();
//			}*/
//			
//				/*if (it->second[0] == cCurrInput) // if the flow is right
//				{
//					m_szStack.erase(m_szStack.begin(), m_szStack.begin() + 1);
//					m_szStack = N0 + m_szStack;
//					goto L_next;
//				}
//				else if (it->second[0] == cN0) // if recursive rule then check next one
//					continue;
//				else if (!isTerminal(it->second[0])) // if other non-terminal then reveal it
//				{
//					tmp = it->second;
//
//					auto _values = m_mmF.equal_range(it->second[0]);
//					for (std::multimap<char, std::string>::iterator _it = _values.first; _it != _values.second; ++_it)
//					{
//						if (isTerminal(_it->second[0]))
//						{
//							tmp.erase(tmp.begin(), tmp.begin() + 1);
//							tmp = _it->second + tmp;
//
//							m_szStack.erase(m_szStack.begin(), m_szStack.begin() + 1);
//							m_szStack = tmp + m_szStack;
//							goto L_next;
//						}
//					}
//				}*/
//			//goto L_exitFailure;
//		}
//		else
//		{
//			int nCurrSize = vStacks.size();
//			for (int iStack = 0; iStack < nCurrSize; iStack++)
//			{
//				printf("[%d] Memory: %s\n", iStack, vStacks[iStack].c_str());
//
//				if (vStacks[iStack][0] == cCurrInput) // if the flow is right
//				{
//					vStacks[iStack].erase(vStacks[iStack].begin(), vStacks[iStack].begin() + 1);
//					vStacks[iStack] = N0 + vStacks[iStack];
//					continue;
//				}
//			}
//		}
//
//		continue;
//
//		//printf("Memory: %s\n", m_szStack.c_str());
//	L_next:
//		printf("\n");
//		i++;
//	}
//
//L_exitFailure:
//	printf("[ERROR] Sequence \"%s\" does not belong to the grammary.\n", a_str.c_str());
//}


//void CPDA::parseTopDownEx(std::string a_str)
//{
//	//bruteForce();
//	//return;
//
//	int i = 0;
//	char cCurrInput, cNext;
//	std::string str = a_str;
//	std::string N0 = "S";
//	char cN0 = 'S';
//
//	m_szStack.clear();
//	m_szStack = N0 + m_szStack;
//
//	for (;;)
//	{
//		cCurrInput = str[i];
//		cN0 = m_szStack[0];
//
//		if (cCurrInput == '\0')
//		{
//			printf("[Input] f\n");
//			printf("Memory: [END]\n");
//			return;
//		}
//		else
//		{
//			//m_szStack = cCurrInput + m_szStack;
//		}
//
//		printf("[Input] %c\n", cCurrInput);
//
//		input_t input;
//		std::string tmp;
//
//		if (!isTerminal(cN0)) // then find suitable value
//		{
//			auto values = m_mmF.equal_range(cN0);
//			for (std::multimap<char, std::string>::iterator it = values.first; it != values.second; ++it)
//			{
//				if (it->second[0] == cCurrInput) // if the flow is right
//				{
//					m_szStack.erase(m_szStack.begin(), m_szStack.begin() + 1);
//					m_szStack = N0 + m_szStack;
//					goto L_next;
//				}
//				else if (it->second[0] == cN0) // if recursive rule then check next one
//					continue;
//				else if (!isTerminal(it->second[0])) // if other non-terminal then reveal it
//				{
//					tmp = it->second;
//
//					auto _values = m_mmF.equal_range(it->second[0]);
//					for (std::multimap<char, std::string>::iterator _it = _values.first; _it != _values.second; ++_it)
//					{
//						if (isTerminal(_it->second[0]))
//						{
//							tmp.erase(tmp.begin(), tmp.begin() + 1);
//							tmp = _it->second + tmp;
//
//							m_szStack.erase(m_szStack.begin(), m_szStack.begin() + 1);
//							m_szStack = tmp + m_szStack;
//							goto L_next;
//						}
//					}
//				}
//			}
//			goto L_exitFailure;
//		}
//		else
//		{
//
//		}
//
//		printf("Memory: %s\n", m_szStack.c_str());
//	L_next:
//		printf("\n");
//		i++;
//	}
//
//L_exitFailure:
//	printf("[ERROR] Sequence \"%s\" does not belong to the grammary.\n", a_str.c_str());
//}
