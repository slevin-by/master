#ifndef _IFSM_H_
#define _IFSM_H_

#include <memory>
#include <vector>
#include <string>
#include <map>
#include "IFsmImport.h"

enum
{
	in,
	cond,
	out,
};

class IFSM
{
public:
	virtual ~IFSM() {}
	virtual bool import(std::shared_ptr<IFsmImport> &fsmi) = 0;
	virtual std::string getNextState(std::string input, char condition) = 0;
	virtual char getNextState(char input, char condition) = 0;

protected:
	virtual bool isExistInQ(char obj) const = 0;
	virtual bool isExistInT(char obj) const = 0;
};

#endif // _IFSM_H_
