#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
#include <fstream>
#include "Grammary.h"
#include "PDA.h"

void test()
{
	CGrammary grammary;

	std::vector<std::string> Vt = { "a", "(", ")", "+", "/", };
	std::vector<std::string> Vn = { "A", };

	grammary.setS("S");
	grammary.setVt(Vt);
	grammary.setVn(Vn);

	grammary.addOneP("S", "S+A");
	grammary.addOneP("S", "S/A");
	grammary.addOneP("S", "A");
	grammary.addOneP("A", "a");
	grammary.addOneP("A", "(S)");

	std::cout << grammary.alalyze() << std::endl << std::endl;

	PbaImportBottomUp pbai = grammary.exportGrammaryEx(true)->convertToPBABA();
	CPDA pba(pbai);
	pba.parseBottomUpEx("a/(a+A)");
}

void test_ex()
{
	CGrammary grammary;

	std::vector<std::string> Vt = { "a", "(", ")", "+", "/", };
	std::vector<std::string> Vn = { "A", };

	grammary.setS("S");
	grammary.setVt(Vt);
	grammary.setVn(Vn);

	grammary.addOneP("S", "S+A");
	grammary.addOneP("S", "S/A");
	grammary.addOneP("S", "A");
	grammary.addOneP("A", "a");
	grammary.addOneP("A", "(S)");

	std::cout << grammary.alalyze() << std::endl << std::endl;

	PbaImportTopDown pbai = grammary.exportGrammaryEx(true)->convertToPBATD();
	CPDA pba(pbai);
	pba.parseTopDownEx("a/(a+a)");
}

void Var()
{
	CGrammary grammary;

	std::vector<std::string> Vt = { "n", "m", "l", "p", "@", "|", };
	std::vector<std::string> Vn = { "L", "M", "P", "N", };

	grammary.setS("S");
	grammary.setVt(Vt);
	grammary.setVn(Vn);

	grammary.addOneP("S", "@nL");
	grammary.addOneP("S", "@mM");
	grammary.addOneP("S", "P");
	grammary.addOneP("L", "M");
	grammary.addOneP("L", "Ll|");
	grammary.addOneP("L", "Lm|");
	grammary.addOneP("L", SZ_EPSILON);
	grammary.addOneP("M", "L");
	grammary.addOneP("M", "Mm");
	grammary.addOneP("M", "mm");
	grammary.addOneP("N", "pN@");
	grammary.addOneP("N", "@");
	grammary.addOneP("P", "nm");

	std::cout << grammary.alalyze() << std::endl;

	PbaImportBottomUp pbai = grammary.exportGrammaryEx(true)->convertToPBABA();
	CPDA pba(pbai);
}

void Var7()
{
	CGrammary grammary;

	std::vector<std::string> Vt = { "n", "m", "l", "p", "@", "|", };
	std::vector<std::string> Vn = { "L", "M", "P", "N", };

	grammary.setS("S");
	grammary.setVt(Vt);
	grammary.setVn(Vn);

	grammary.addOneP("S", "@nL");
	grammary.addOneP("S", "@mM");
	grammary.addOneP("S", "P");
	grammary.addOneP("L", "M");
	grammary.addOneP("L", "MZ");
	grammary.addOneP("M", "LY");
	grammary.addOneP("M", "mmY");
	grammary.addOneP("M", "mm");
	grammary.addOneP("N", "nm");
	grammary.addOneP("Z", "l|");
	grammary.addOneP("Z", "l|Z");
	grammary.addOneP("Z", "m|Z");
	grammary.addOneP("Z", "m|");
	grammary.addOneP("Y", "mY");
	grammary.addOneP("Y", "m");
	grammary.addOneP("P", "nm");

	PbaImportBottomUp pbai = grammary.exportGrammaryEx(true)->convertToPBABA();
	CPDA pba(pbai);
	pba.parseBottomUpEx("mm");
}

void Var7ex()
{
	CGrammary grammary;

	std::vector<std::string> Vt = { "n", "m", "l", "p", "@", "|", };
	std::vector<std::string> Vn = { "L", "M", "P", "N", };

	grammary.setS("S");
	grammary.setVt(Vt);
	grammary.setVn(Vn);

	grammary.addOneP("S", "@nL");
	grammary.addOneP("S", "@mM");
	grammary.addOneP("S", "P");
	grammary.addOneP("L", "M");
	grammary.addOneP("L", "MZ");
	grammary.addOneP("M", "LY");
	grammary.addOneP("M", "mmY");
	grammary.addOneP("M", "mm");
	grammary.addOneP("N", "nm");
	grammary.addOneP("Z", "l|");
	grammary.addOneP("Z", "l|Z");
	grammary.addOneP("Z", "m|Z");
	grammary.addOneP("Z", "m|");
	grammary.addOneP("Y", "mY");
	grammary.addOneP("Y", "m");
	grammary.addOneP("P", "nm");

	PbaImportTopDown pbai = grammary.exportGrammaryEx(true)->convertToPBATD();
	CPDA pba(pbai);
	pba.parseTopDownEx("@nmml|");
}

int main(int argc, char **argv)
{
	// Bottom-Up
	//test();
	//std::cout << "\n**************************************\n";
	//Var7();

	// Top-Down
	test_ex();
	std::cout << "\n**************************************\n";
	Var7ex();

	return 0;
}
