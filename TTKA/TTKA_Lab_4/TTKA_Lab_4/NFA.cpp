#include "NFA.h"
#include "NfaImport.h"

#include "DFA.h"
#include <queue>
#include "DfaImport.h"

#include <algorithm>
#include <set>

bool CNFA::import(std::shared_ptr<IFsmImport> &rFsmi)
{
	if (!rFsmi.get())
		return false;

	CNfaImport *smi = dynamic_cast<CNfaImport *>(rFsmi.get());
	m_Q = smi->Q;
	m_T = smi->T;
	m_F = smi->F;
	m_H = smi->H;
	m_Z = smi->Z;

	return true;
}

std::string CNFA::getNextState(std::string input, char condition)
{
	if (!isExistInQ(input[0]) || !isExistInT(condition))
		return "";

	std::string tmp;
	for (auto it : m_F)
	{
		if ((std::get<in>(it) == input[0]) && (std::get<cond>(it) == condition))
			tmp.append(std::string(1, std::get<out>(it)));
	}
	return tmp;
}

char CNFA::getNextState(char input, char condition)
{
	if (!isExistInQ(input) || !isExistInT(condition))
		return '\0';

	std::string tmp;
	for (auto it : m_F)
	{
		if ((std::get<in>(it) == input) && (std::get<cond>(it) == condition))
			return std::get<out>(it);
	}
	return '\0';
}

std::shared_ptr<IFsmImport> CNFA::toDFA()
{	
	std::shared_ptr<IFsmImport> _dfaImport(new CDfaImport());
	CDfaImport *dfaImport = dynamic_cast<CDfaImport *>(_dfaImport.get());
	dfaImport->T = m_T;
	dfaImport->H = m_H;

	auto makeNewState = [](char c)
	{
		return std::string(1, c);
	};

	std::queue<std::string> Q;
	std::vector<std::string> pool;

	std::string state;
	state.reserve(2);

	std::string szInput;
	szInput.reserve(2);

	state = makeNewState(m_H);
	Q.push(state);
	pool.push_back(state);
	//m_Qd.push_back(state);

	for (;;)
	{
		std::string qd = Q.front();
		Q.pop();

		for (char condition : m_T)
		{
			state.clear();

			for (char input : qd)
			{
				szInput = input;
				state.append(getNextState(szInput, condition));
			}

			// sort symbols in the state name
			if ((state.size() > 1) && (state[1] < state[0]))
				std::swap(state[0], state[1]);

			if (!state.empty())
			{
				dfaImport->Fd.push_back(CDfaImport::func_t(qd, condition, state));
				if (std::find(pool.begin(), pool.end(), state) == pool.end())
				{
					Q.push(state);
					pool.push_back(state);
				}
			}
		}

		if (Q.empty())
			break;
	}

	dfaImport->Qd = pool;

	for (auto it : pool)
	{
		if (it.find(m_Z) != std::string::npos)
			dfaImport->Zd.push_back(it);
	}

	return _dfaImport;
}

std::vector<char> CNFA::getUpdatedInputs()
{
	return m_Q;
}

void CNFA::deleteUnreachable()
{
	std::vector<char> Q;
	Q.push_back(m_H);

	std::vector<char> Unreachable;
	std::vector<func_t> UnreachableF;

	auto ExistInQ = [&Q](char c)
	{
		for (char q : Q)
		{
			if (c == q)
				return true;
		}
		return false;
	};

	for (char cInput : m_Q)
	{
		if (!ExistInQ(cInput))
		{
			Unreachable.push_back(cInput);
			for (char cond : m_T)
			{
				char cOutput = getNextState(cInput, cond);
				if (cOutput != '\0')
					UnreachableF.push_back(func_t(cInput, cond, cOutput));
			}
			continue;
		}

		for (char cond : m_T)
		{
			char cOutput = getNextState(cInput, cond);
			if (cOutput != '\0' && !ExistInQ(cOutput))
				Q.push_back(cOutput);
		}
	}

	// Delete
	for (char c : Unreachable)
		m_Q.erase(std::remove(m_Q.begin(), m_Q.end(), c), m_Q.end());
	for (auto f : UnreachableF)
		m_F.erase(std::remove(m_F.begin(), m_F.end(), f), m_F.end());
}

void CNFA::deleteEquivalents()
{
	std::vector<std::string> prevR, R;
	std::vector<std::string> m_realGroups;

	std::vector<char> vAlreadySplitted;

	auto ExistInStr = [](std::string &s, char c)
	{
		for (char cs : s)
		{
			if (c == cs)
				return true;
		}
		return false;
	};
	auto GetGroupId = [&m_realGroups, &ExistInStr](char c)
	{
		//for (int iGroup = 0; iGroup < prevR.size(); iGroup++)
		//{
		//	if (ExistInStr(prevR[iGroup], c))
		//		return iGroup;
		//}
		//return -1;
		for (int iGroup = 0; iGroup < m_realGroups.size(); iGroup++)
		{
			if (ExistInStr(m_realGroups[iGroup], c))
				return iGroup;
		}
		return -1;
	};
	auto SplitFrom = [&ExistInStr](std::vector<std::string> &groups, char obj)
	{
		std::vector<std::string> newGroups;
		for (auto &group : groups)
		{
			if (ExistInStr(group, obj))
			{
				std::string newGroup;
				for (char c : group)
				{
					if (c == obj)
						newGroups.push_back(std::string(1, c));
					else
						newGroup += c;
				}
				newGroups.push_back(newGroup);
			}
			else
				newGroups.push_back(group);
		}
		groups.swap(newGroups);
	};
	auto SplitFromEx = [&ExistInStr](std::vector<std::string> &groups, char obj, std::set<char> &neigbours)
	{
		std::vector<std::string> newGroups;
		for (auto &group : groups)
		{
			if (ExistInStr(group, obj))
			{
				std::string newGroup;
				for (char c : group)
				{
					if (c == obj)
					{
						neigbours.insert(c);
						std::string _g;
						for (auto &ng : neigbours)
							_g += ng;
						newGroups.push_back(_g);
					}
					if (neigbours.find(c) != neigbours.end())
						continue;
					else
						newGroup += c;
				}
				if (!newGroup.empty())
					newGroups.push_back(newGroup);
			}
			else
				newGroups.push_back(group);
		}
		groups.swap(newGroups);
	};
	
	std::string tmp1, tmp2;
	for (char c : m_Q)
	{
		if (!isExistInZ(c))
			tmp1 += c;
		else
			tmp2 += c;
	}

	R.push_back(tmp1);
	R.push_back(tmp2);

	prevR = R;
	m_realGroups = prevR;

	for (int i = 0; i < m_Q.size(); i++)
	{
		std::map<char, int> mapGroups; // condition:group for the current input
		int nCurrentCondGroup = 0; // group for the current condition of current input
		std::vector<char> vGroupMates;
		std::vector<std::vector<char>> vGroupMatesForColumn;
		std::set<char> vNewNeighbors;

		bool bIsEarlyBreak = false;

		for (char cond : m_T)
		{
			char cOut = getNextState(m_Q[i], cond);
			if (cOut != '\0')
			{
				// Get the group for this condition
				nCurrentCondGroup = GetGroupId(cOut);
				mapGroups.insert(std::make_pair(cond, nCurrentCondGroup));
			}
			else
				continue;

			// Get the group for other inputs
			if (i == m_Q.size() - 1)
				break;

			for (int j = i; j < m_Q.size(); j++)
			{
				char cInput = m_Q[j];
				char cOutput = getNextState(cInput, cond);
				if (cOutput != '\0')
				{
					if (GetGroupId(cOutput) != mapGroups[cond])
						break;
					else
						vGroupMates.push_back(cOutput);
				}
			}

			// IMPORTANT CHECK
			if (vGroupMates.size() == 1)
			{
				bIsEarlyBreak = true;
				break;
			}

			// Check if groupmates for this condition are belonged to the same group
			for (auto &groupMate : vGroupMates)
			{
				if (GetGroupId(groupMate) != mapGroups[cond])
					goto L_NextInput;
			}

			// Add neighbours
			for (int j = i + 1; j < m_Q.size(); j++)
			{
				char cInput = m_Q[j];
				char cOutput = getNextState(cInput, cond);
				if (cOutput != '\0')
					vNewNeighbors.insert(cInput);
			}

			// Clean groupmates vector
			vGroupMatesForColumn.push_back(vGroupMates);
			mapGroups.clear();
			vGroupMates.clear();
		}

		char current = m_Q[i];
		if (std::find(vAlreadySplitted.begin(), vAlreadySplitted.end(),current) == vAlreadySplitted.end())
		{
			if (bIsEarlyBreak)
			{
				vAlreadySplitted.push_back(m_Q[i]);

				mapGroups.clear();
				vGroupMates.clear();

				// If we got there then put the current elemnt into separate group
				SplitFrom(R, m_Q[i]);
				prevR = R;
			}
			else
			{
				vAlreadySplitted.push_back(m_Q[i]);
				for (auto it : vNewNeighbors)
					vAlreadySplitted.push_back(it);

				mapGroups.clear();
				vGroupMates.clear();

				SplitFromEx(R, m_Q[i], vNewNeighbors);

				prevR = R;
			}
		}

	L_NextInput:
		mapGroups.clear();
		vGroupMates.clear();
		vGroupMatesForColumn.clear();
		vNewNeighbors.clear();
		__asm nop;
	}

	std::vector<std::string> newStrQ;
	for (auto &it : R)
		newStrQ.push_back(it);

	std::vector<char> newQ;
	std::map<char, char> mapOldNew;
	for (std::string s : newStrQ)
	{
		newQ.push_back(s[0]);
		for (char c : s)
			mapOldNew.insert(std::make_pair(c, s[0]));
	}

	std::vector<func_t> newF;
	for (std::string szIn : newStrQ)
	{
		for (char cond : m_T)
		{
			for (char cIn : szIn)
			{
				char cOut = getNextState(cIn, cond);
				if (cOut != '\0')
				{
					auto rule = func_t(mapOldNew[cIn], cond, mapOldNew[cOut]);
					if (std::find(newF.begin(), newF.end(), rule) == newF.end())
						newF.emplace_back(rule);
				}
			}
		}
	}

	m_Q.swap(newQ);
	m_F.swap(newF);
}

bool CNFA::isExistInQ(char obj) const
{
	return std::find(m_Q.cbegin(), m_Q.cend(), obj) != m_Q.cend();
}

bool CNFA::isExistInT(char obj) const
{
	return std::find(m_T.cbegin(), m_T.cend(), obj) != m_T.cend();
}

bool CNFA::isExistInZ(char obj) const
{
	return std::find(m_Zn.cbegin(), m_Zn.cend(), obj) != m_Zn.cend();
}
