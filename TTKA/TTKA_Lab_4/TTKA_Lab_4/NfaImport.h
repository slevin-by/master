#ifndef _NFA_IMPORT_H_
#define _NFA_IMPORT_H_

#include "IFsmImport.h"
#include <tuple>
#include <vector>

class CNfaImport : public IFsmImport
{
public:
	using func_t = std::tuple<char, char, char>;

	CNfaImport() = default;

	std::vector<char> Q;	// Vn
	std::vector<char> T;	// Vt
	std::vector<func_t> F; // P
	char H; // S
	char Z; // N
};

#endif // _NFA_IMPORT_H_
