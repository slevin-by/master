#ifndef _PDA_H_
#define _PDA_H_

#include <vector>
#include <map>
#include <tuple>

#include "Memory.h"
#include "GrammaryExport.h"
#include "Grammary.h"

// Pushdown Automaton
class CPDA
{
	using alphabeth_t = std::vector<char>;

	using input_t = std::tuple<char, char, std::string>;
	using output_t = std::pair<char, char>;
	using func_t = std::map<input_t, output_t>;
	using vfunc_t = std::vector<std::pair<std::string, char>>;

public:
	CPDA();
	CPDA(PbaImportBottomUp &pbai);
	CPDA(PbaImportTopDown &pbai);
	~CPDA();

	void parseBottomUp(std::string str);
	void parseBottomUpEx(std::string str);
	void parseTopDown(std::string str);
	void parseTopDownEx(std::string str);

private:
	void import(PbaImportBottomUp &pbai);

	bool isTerminal(char a_c)
	{
		for (char c : m_Vt)
		{
			if (c == a_c)
				return true;
		}
		return false;
	}

	std::string findTerminal(char nt);
	void bruteForce();

private:
	func_t m_F;
	vfunc_t m_vF;

	char m_q0;
	char m_N0;

	std::string m_szStack;

	// Top-Down
	std::multimap<char, std::string> m_mmF;
	std::vector<char> m_Vn;
	std::vector<char> m_Vt;
};

#endif // _PDA_H_