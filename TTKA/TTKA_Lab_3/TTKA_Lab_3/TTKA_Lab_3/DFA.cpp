#include "DFA.h"
#include "DfaImport.h"

#include <algorithm>

bool CDFA::import(std::shared_ptr<IFsmImport> &rFsmi)
{
	if (!rFsmi.get())
		return false;

	CDfaImport *smi = dynamic_cast<CDfaImport *>(rFsmi.get());
	m_Qd = smi->Qd;
	m_T = smi->T;
	m_Fd = smi->Fd;
	m_H = smi->H;
	m_Zd = smi->Zd;

	char charPool = 'A';
	for (std::string s : m_Qd)
	{
		if (s.size() > 1)
		{
			while (isExistInQ(charPool) && (charPool < 'Z'))
				charPool++;
			if (charPool > 'Z')
				return false;
			m_mapNameSz.insert(std::make_pair(s, charPool));
			m_mapNameCh.insert(std::make_pair(charPool, s));
			charPool++;
		}
		else
		{
			m_mapNameSz.insert(std::make_pair(s, s[0]));
			m_mapNameCh.insert(std::make_pair(s[0], s));
		}
	}

	return true;
}

std::string CDFA::getNextState(std::string input, char condition)
{
	if (m_Fd.empty())
		return "";

	if (!isExistInQd(input) || !isExistInT(condition))
		return "";

	std::string tmp;
	for (auto it : m_Fd)
	{
		if ((std::get<in>(it) == input) && (std::get<cond>(it) == condition))
			return std::get<out>(it);
	}
	return "";
}

char CDFA::getNextState(char input, char condition)
{
	if (m_Fd.empty())
		return '\0';

	if (!isExistInQ(input) || !isExistInT(condition))
		return '\0';

	std::string szRealNode = m_mapNameCh[input];

	std::string tmp;
	for (auto it : m_Fd)
	{
		if ((std::get<in>(it) == szRealNode) && (std::get<cond>(it) == condition))
			return m_mapNameSz[std::get<out>(it)];
	}
	return '\0';
}

std::vector<char> CDFA::getUpdatedInputs()
{
	std::vector<char> v;
	for (auto s : m_Qd)
	{
		if (s.size() > 1)
			v.push_back(m_mapNameSz[s]);
		else
			v.push_back(s[0]);
	}
	return v;
}

std::vector<char> CDFA::getUpdatedFinish()
{
	std::vector<char> v;
	for (auto s : m_Zd)
	{
		if (s.size() > 1)
			v.push_back(m_mapNameSz[s]);
		else
			v.push_back(s[0]);
	}
	return v;
}

void CDFA::deleteUnreachable()
{
	std::vector<char> Q;
	Q.push_back(m_H);

	std::vector<char> Unreachable;

	auto ExistInQ = [&Q](char c) 
	{
		for (char q : Q)
		{
			if (c == q)
				return true;
		}
		return false;
	};

	for (std::string input : m_Qd)
	{
		char cInput = m_mapNameSz[input];
		if (!ExistInQ(cInput))
		{
			Unreachable.push_back(cInput);
			continue;
		}
		
		for (char cond : m_T)
		{
			char cOutput = getNextState(cInput, cond);
			if (cOutput != '\0' && !ExistInQ(cOutput))
				Q.push_back(cOutput);
		}
	}

	// Delete
	for (char c : Unreachable)
	{
		m_Qd.erase(std::remove(m_Qd.begin(), m_Qd.end(), m_mapNameCh[c]), m_Qd.end());
	}
}

bool CDFA::isExistInQ(char obj) const
{
	bool bQ = std::find(m_Qd.cbegin(), m_Qd.cend(), std::string(1, obj)) != m_Qd.cend();
	bool bMap = (m_mapNameCh.find(obj) != m_mapNameCh.end());
	return bQ | bMap;
}

bool CDFA::isExistInT(char obj) const
{
	return std::find(m_T.cbegin(), m_T.cend(), obj) != m_T.cend();
}

bool CDFA::isExistInQd(std::string obj) const
{
	return std::find(m_Qd.cbegin(), m_Qd.cend(), obj) != m_Qd.cend();
}
