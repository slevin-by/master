#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
#include <fstream>
#include "Grammary.h"
#include "StateMachine.h"

#include "GrammaryExport.h"

void test()
{
	CGrammary grammary;

	std::vector<std::string> Vt = { "a", "b", };
	std::vector<std::string> Vn = { "B", "C", "D", "E", "F", "G", };

	grammary.setS("A");
	grammary.setVt(Vt);
	grammary.setVn(Vn);

	grammary.addOneP("A", "aB");
	grammary.addOneP("A", "bC");
	grammary.addOneP("B", "bD");
	grammary.addOneP("C", "bE");
	grammary.addOneP("D", "aC");
	grammary.addOneP("D", "bE");
	grammary.addOneP("E", "aB");
	grammary.addOneP("E", "bD");
	grammary.addOneP("F", "aD");
	grammary.addOneP("F", "bG");
	grammary.addOneP("G", "aF");
	grammary.addOneP("G", "bE");

	std::cout << grammary.alalyze() << std::endl;

	std::shared_ptr<GrammaryExport> spGe = grammary.exportGrammaryEx();

	Fsmfactory factory;
	factory.init(spGe);

	std::shared_ptr<IFSM> spNfa = factory.create(eFactoryNfa);
	dynamic_cast<CNFA *>(spNfa.get())->addZeros({ 'D', 'E' });

	CNFA *pNfa = dynamic_cast<CNFA *>(spNfa.get());
	pNfa->deleteUnreachable();
	pNfa->deleteEquivalents();

	CDFA *pDfa = new CDFA();
	pDfa->import(pNfa->toDFA());

	//std::vector<char> inputs = dynamic_cast<CNFA *>(spNfa.get())->getUpdatedInputs();
	std::vector<char> inputs = pDfa->getUpdatedInputs();

	std::ofstream ofs("serial.txt", std::ofstream::out | std::ofstream::trunc);
	if (!ofs.is_open())
		return;

	for (char c : inputs)
	{
		for (auto szW : Vt)
		{
			char out = spNfa->getNextState(c, szW[0]);
			if (out != '\0')
				ofs << "[('" << c << "','" << out << "')]"
				<< ":" << (int)(szW[0] - '0') << std::endl;
		}
	}

	ofs.close();
}

void Var1()
{
	CGrammary grammary;

	std::vector<std::string> Vt = { "0", "1", "a", "b", "*", "/", "-", };
	std::vector<std::string> Vn = { "A", "B", "C", "D", "E", "F", "G", "N", };

	grammary.setS("S");
	grammary.setVt(Vt);
	grammary.setVn(Vn);

	grammary.addOneP("S", "0A");
	grammary.addOneP("S", "1B");
	grammary.addOneP("A", "*E");
	grammary.addOneP("B", "/D");
	grammary.addOneP("C", "aB");
	grammary.addOneP("C", "bE");
	grammary.addOneP("D", "0F");
	grammary.addOneP("D", "aG");
	grammary.addOneP("D", "bE");
	grammary.addOneP("E", "0N");
	grammary.addOneP("E", "aG");
	grammary.addOneP("E", "bD");
	grammary.addOneP("F", "-D");
	grammary.addOneP("N", "-E");

	std::cout << grammary.alalyze() << std::endl;

	std::shared_ptr<GrammaryExport> spGe = grammary.exportGrammaryEx();

	Fsmfactory factory;
	factory.init(spGe);

	std::shared_ptr<IFSM> spNfa = factory.create(eFactoryNfa);
	dynamic_cast<CNFA *>(spNfa.get())->addZeros({ 'G' });

	CNFA *pNfa = dynamic_cast<CNFA *>(spNfa.get());
	pNfa->deleteUnreachable();
	pNfa->deleteEquivalents();

	CDFA *pDfa = new CDFA();
	pDfa->import(pNfa->toDFA());

	std::vector<char> inputs = pDfa->getUpdatedInputs();

	std::ofstream ofs("serial.txt", std::ofstream::out | std::ofstream::trunc);
	if (!ofs.is_open())
		return;

	for (char c : inputs)
	{
		for (auto szW : Vt)
		{
			char out = spNfa->getNextState(c, szW[0]);
			if (out != '\0')
				ofs << "[('" << c << "','" << out << "')]"
				<< ":" << (int)(szW[0] - '0') << std::endl;
		}
	}

	ofs.close();
}

int main(int argc, char **argv)
{
	//test();
	Var1();

	return 0;
}
