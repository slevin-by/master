#ifndef _NFA_H_
#define _NFA_H_

#include "IFSM.h"

class CNFA : public IFSM
{
	using func_t = std::tuple<char, char, char>;

public:
	CNFA() {}

	bool import(std::shared_ptr<IFsmImport> &rFsmi);

	std::string getNextState(std::string input, char condition);
	char getNextState(char input, char condition);

	std::shared_ptr<IFsmImport> toDFA();

	// Define zeros manually
	void addZeros(std::initializer_list<const char> &&vc)
	{
		m_Zn.reserve(vc.size());
		for (char c : vc)
			m_Zn.emplace_back(c);
	}

	std::vector<char> getUpdatedInputs();

	// Minimization
	void deleteUnreachable();
	void deleteEquivalents();

private:
	bool isExistInQ(char obj) const;
	bool isExistInT(char obj) const;
	bool isExistInZ(char obj) const;

private:
	std::vector<char> m_Q;
	std::vector<char> m_T;
	std::vector<func_t> m_F;
	char m_H;
	char m_Z;

	std::vector<char> m_Zn;

	std::map<std::string, char> m_mapNameSz;
	std::map<char, std::string> m_mapNameCh;
};

#endif // _NFA_H_
