import numpy as np
import cv2
import sys

if __name__ == '__main__' :
    tracker = cv2.MultiTracker_create()

    video = cv2.VideoCapture("street_small.mp4")
    if not video.isOpened():
        print "Could not open video"
        sys.exit()

    ok, frame = video.read()
    if not ok:
        print 'Cannot read video file'
        sys.exit()

    init_once = False

    bbox1 = (287, 23, 86, 320)
    bbox2 = (287, 23, 86, 320)
    bbox3 = (287, 23, 86, 320)

    bbox1 = cv2.selectROI(frame, False)
    bbox2 = cv2.selectROI(frame, False)
    bbox3 = cv2.selectROI(frame, False)

    while video.isOpened():
        ok, frame=video.read()
        if not ok:
            print 'no image to read'
            break

        if not init_once:
            ok = tracker.add(cv2.TrackerMIL_create(), frame, bbox1)
            ok = tracker.add(cv2.TrackerMIL_create(), frame, bbox2)
            ok = tracker.add(cv2.TrackerMIL_create(), frame, bbox3)
            init_once = True

        timer = cv2.getTickCount() # now

        ok, boxes = tracker.update(frame)
        #print ok, boxes

        fps = cv2.getTickFrequency() / (cv2.getTickCount() - timer); # fps

        if ok:
            for newbox in boxes:
                p1 = (int(newbox[0]), int(newbox[1]))
                p2 = (int(newbox[0] + newbox[2]), int(newbox[1] + newbox[3]))
                cv2.rectangle(frame, p1, p2, (200,0,0))
        else :
            cv2.putText(frame, "Tracking failure detected", (100,80), cv2.FONT_HERSHEY_SIMPLEX, 0.75,(0,0,255),2)

        cv2.putText(frame, "FPS : " + str(int(fps)), (100,20), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50), 2);
        cv2.imshow('Tracking', frame)

        k = cv2.waitKey(1) & 0xff
        if k == 27 : break
